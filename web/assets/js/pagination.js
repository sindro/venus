$(function(){ 
    var $container = $('#elements');
    
    $(".pagination ul li a").on('click', function(e){
        var href = $(this).attr('href');
        var option = $.deparam.querystring( href );
        $('html, body').animate({scrollTop:0}, 500);
        $.bbq.pushState( { page: option.page, layout: option.layout } );

        e.preventDefault();

        return false;
    });
    
    $(window).bind( 'hashchange', function( event ){
        $('.mask-loader').css({'display': 'block','opacity': 0.95});
        var hashOptions = $.deparam.fragment();
        var page = hashOptions.page != undefined ? hashOptions.page : 1;
        var layout = hashOptions.layout != undefined ? hashOptions.layout : '';
        
        if(hashOptions.layout == 'grid') {
            $('.layout-switcher .grid').addClass('active');
            $('.layout-switcher .list').removeClass('active');
        } else {
            $('.layout-switcher .list').addClass('active');
            $('.layout-switcher .grid').removeClass('active');
        }
        
        $.ajax({
            type: "GET",
            url: $container.data('href'),
            data: { page: page, layout: layout }
        })
        .done(function( content ) {
            $('.mask-loader').css({'display': 'none','opacity': .0});
    
            $container.html($(content).find('#elements').html());
            $('.show_results').html($(content).find('.show_results').html());
            $('.pagination').html($(content).find('.pagination').html());

            $(".pagination ul li a").on('click', function(e){
                var href = $(this).attr('href');
                var option = $.deparam.querystring( href );
                $('html, body').animate({scrollTop:0}, 500);
                $.bbq.pushState( { page: option.page, layout: option.layout } );

                e.preventDefault();

                return false;
            });
        });
    }).trigger('hashchange');
});

