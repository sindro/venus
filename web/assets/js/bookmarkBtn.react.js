class LikeButton extends React.Component {
  constructor() {
    super();
    this.state = {
      liked: false
    };
    this.handleClick = this.handleClick.bind(this);
  } 
  
  handleClick() {
    this.setState({
      liked: !this.state.liked
    });
  }
  
  render() {
    const text = this.state.liked ? 'liked' : 'haven\'t liked';
    const label = this.state.liked ? 'Unlike' : 'Like'
    return (
        <button className="like-button">
            <span className="like-icon"></span> Bookmark this listing\n\
        </button> 
	<span>159 people bookmarked this place</span>
    );
  }
}

ReactDOM.render(
  <LikeButton />,
  document.getElementById('bookmark')
)