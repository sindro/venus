var $ = require('jquery');
require('bootstrap-sass');
require('jpanel-menu');
require('chosen-js');
require('magnific-popup');
require('slick-carousel');

/*! jQuery UI - v1.12.1 - 2017-05-10
* http://jqueryui.com
* Includes: widget.js, data.js, scroll-parent.js, widgets/sortable.js, widgets/mouse.js
* Copyright jQuery Foundation and other contributors; Licensed MIT */

(function(t){"function"==typeof define&&define.amd?define(["jquery"],t):t(jQuery)})(function(t){t.ui=t.ui||{},t.ui.version="1.12.1";var e=0,i=Array.prototype.slice;t.cleanData=function(e){return function(i){var s,n,o;for(o=0;null!=(n=i[o]);o++)try{s=t._data(n,"events"),s&&s.remove&&t(n).triggerHandler("remove")}catch(a){}e(i)}}(t.cleanData),t.widget=function(e,i,s){var n,o,a,r={},l=e.split(".")[0];e=e.split(".")[1];var h=l+"-"+e;return s||(s=i,i=t.Widget),t.isArray(s)&&(s=t.extend.apply(null,[{}].concat(s))),t.expr[":"][h.toLowerCase()]=function(e){return!!t.data(e,h)},t[l]=t[l]||{},n=t[l][e],o=t[l][e]=function(t,e){return this._createWidget?(arguments.length&&this._createWidget(t,e),void 0):new o(t,e)},t.extend(o,n,{version:s.version,_proto:t.extend({},s),_childConstructors:[]}),a=new i,a.options=t.widget.extend({},a.options),t.each(s,function(e,s){return t.isFunction(s)?(r[e]=function(){function t(){return i.prototype[e].apply(this,arguments)}function n(t){return i.prototype[e].apply(this,t)}return function(){var e,i=this._super,o=this._superApply;return this._super=t,this._superApply=n,e=s.apply(this,arguments),this._super=i,this._superApply=o,e}}(),void 0):(r[e]=s,void 0)}),o.prototype=t.widget.extend(a,{widgetEventPrefix:n?a.widgetEventPrefix||e:e},r,{constructor:o,namespace:l,widgetName:e,widgetFullName:h}),n?(t.each(n._childConstructors,function(e,i){var s=i.prototype;t.widget(s.namespace+"."+s.widgetName,o,i._proto)}),delete n._childConstructors):i._childConstructors.push(o),t.widget.bridge(e,o),o},t.widget.extend=function(e){for(var s,n,o=i.call(arguments,1),a=0,r=o.length;r>a;a++)for(s in o[a])n=o[a][s],o[a].hasOwnProperty(s)&&void 0!==n&&(e[s]=t.isPlainObject(n)?t.isPlainObject(e[s])?t.widget.extend({},e[s],n):t.widget.extend({},n):n);return e},t.widget.bridge=function(e,s){var n=s.prototype.widgetFullName||e;t.fn[e]=function(o){var a="string"==typeof o,r=i.call(arguments,1),l=this;return a?this.length||"instance"!==o?this.each(function(){var i,s=t.data(this,n);return"instance"===o?(l=s,!1):s?t.isFunction(s[o])&&"_"!==o.charAt(0)?(i=s[o].apply(s,r),i!==s&&void 0!==i?(l=i&&i.jquery?l.pushStack(i.get()):i,!1):void 0):t.error("no such method '"+o+"' for "+e+" widget instance"):t.error("cannot call methods on "+e+" prior to initialization; "+"attempted to call method '"+o+"'")}):l=void 0:(r.length&&(o=t.widget.extend.apply(null,[o].concat(r))),this.each(function(){var e=t.data(this,n);e?(e.option(o||{}),e._init&&e._init()):t.data(this,n,new s(o,this))})),l}},t.Widget=function(){},t.Widget._childConstructors=[],t.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{classes:{},disabled:!1,create:null},_createWidget:function(i,s){s=t(s||this.defaultElement||this)[0],this.element=t(s),this.uuid=e++,this.eventNamespace="."+this.widgetName+this.uuid,this.bindings=t(),this.hoverable=t(),this.focusable=t(),this.classesElementLookup={},s!==this&&(t.data(s,this.widgetFullName,this),this._on(!0,this.element,{remove:function(t){t.target===s&&this.destroy()}}),this.document=t(s.style?s.ownerDocument:s.document||s),this.window=t(this.document[0].defaultView||this.document[0].parentWindow)),this.options=t.widget.extend({},this.options,this._getCreateOptions(),i),this._create(),this.options.disabled&&this._setOptionDisabled(this.options.disabled),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:function(){return{}},_getCreateEventData:t.noop,_create:t.noop,_init:t.noop,destroy:function(){var e=this;this._destroy(),t.each(this.classesElementLookup,function(t,i){e._removeClass(i,t)}),this.element.off(this.eventNamespace).removeData(this.widgetFullName),this.widget().off(this.eventNamespace).removeAttr("aria-disabled"),this.bindings.off(this.eventNamespace)},_destroy:t.noop,widget:function(){return this.element},option:function(e,i){var s,n,o,a=e;if(0===arguments.length)return t.widget.extend({},this.options);if("string"==typeof e)if(a={},s=e.split("."),e=s.shift(),s.length){for(n=a[e]=t.widget.extend({},this.options[e]),o=0;s.length-1>o;o++)n[s[o]]=n[s[o]]||{},n=n[s[o]];if(e=s.pop(),1===arguments.length)return void 0===n[e]?null:n[e];n[e]=i}else{if(1===arguments.length)return void 0===this.options[e]?null:this.options[e];a[e]=i}return this._setOptions(a),this},_setOptions:function(t){var e;for(e in t)this._setOption(e,t[e]);return this},_setOption:function(t,e){return"classes"===t&&this._setOptionClasses(e),this.options[t]=e,"disabled"===t&&this._setOptionDisabled(e),this},_setOptionClasses:function(e){var i,s,n;for(i in e)n=this.classesElementLookup[i],e[i]!==this.options.classes[i]&&n&&n.length&&(s=t(n.get()),this._removeClass(n,i),s.addClass(this._classes({element:s,keys:i,classes:e,add:!0})))},_setOptionDisabled:function(t){this._toggleClass(this.widget(),this.widgetFullName+"-disabled",null,!!t),t&&(this._removeClass(this.hoverable,null,"ui-state-hover"),this._removeClass(this.focusable,null,"ui-state-focus"))},enable:function(){return this._setOptions({disabled:!1})},disable:function(){return this._setOptions({disabled:!0})},_classes:function(e){function i(i,o){var a,r;for(r=0;i.length>r;r++)a=n.classesElementLookup[i[r]]||t(),a=e.add?t(t.unique(a.get().concat(e.element.get()))):t(a.not(e.element).get()),n.classesElementLookup[i[r]]=a,s.push(i[r]),o&&e.classes[i[r]]&&s.push(e.classes[i[r]])}var s=[],n=this;return e=t.extend({element:this.element,classes:this.options.classes||{}},e),this._on(e.element,{remove:"_untrackClassesElement"}),e.keys&&i(e.keys.match(/\S+/g)||[],!0),e.extra&&i(e.extra.match(/\S+/g)||[]),s.join(" ")},_untrackClassesElement:function(e){var i=this;t.each(i.classesElementLookup,function(s,n){-1!==t.inArray(e.target,n)&&(i.classesElementLookup[s]=t(n.not(e.target).get()))})},_removeClass:function(t,e,i){return this._toggleClass(t,e,i,!1)},_addClass:function(t,e,i){return this._toggleClass(t,e,i,!0)},_toggleClass:function(t,e,i,s){s="boolean"==typeof s?s:i;var n="string"==typeof t||null===t,o={extra:n?e:i,keys:n?t:e,element:n?this.element:t,add:s};return o.element.toggleClass(this._classes(o),s),this},_on:function(e,i,s){var n,o=this;"boolean"!=typeof e&&(s=i,i=e,e=!1),s?(i=n=t(i),this.bindings=this.bindings.add(i)):(s=i,i=this.element,n=this.widget()),t.each(s,function(s,a){function r(){return e||o.options.disabled!==!0&&!t(this).hasClass("ui-state-disabled")?("string"==typeof a?o[a]:a).apply(o,arguments):void 0}"string"!=typeof a&&(r.guid=a.guid=a.guid||r.guid||t.guid++);var l=s.match(/^([\w:-]*)\s*(.*)$/),h=l[1]+o.eventNamespace,c=l[2];c?n.on(h,c,r):i.on(h,r)})},_off:function(e,i){i=(i||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,e.off(i).off(i),this.bindings=t(this.bindings.not(e).get()),this.focusable=t(this.focusable.not(e).get()),this.hoverable=t(this.hoverable.not(e).get())},_delay:function(t,e){function i(){return("string"==typeof t?s[t]:t).apply(s,arguments)}var s=this;return setTimeout(i,e||0)},_hoverable:function(e){this.hoverable=this.hoverable.add(e),this._on(e,{mouseenter:function(e){this._addClass(t(e.currentTarget),null,"ui-state-hover")},mouseleave:function(e){this._removeClass(t(e.currentTarget),null,"ui-state-hover")}})},_focusable:function(e){this.focusable=this.focusable.add(e),this._on(e,{focusin:function(e){this._addClass(t(e.currentTarget),null,"ui-state-focus")},focusout:function(e){this._removeClass(t(e.currentTarget),null,"ui-state-focus")}})},_trigger:function(e,i,s){var n,o,a=this.options[e];if(s=s||{},i=t.Event(i),i.type=(e===this.widgetEventPrefix?e:this.widgetEventPrefix+e).toLowerCase(),i.target=this.element[0],o=i.originalEvent)for(n in o)n in i||(i[n]=o[n]);return this.element.trigger(i,s),!(t.isFunction(a)&&a.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},t.each({show:"fadeIn",hide:"fadeOut"},function(e,i){t.Widget.prototype["_"+e]=function(s,n,o){"string"==typeof n&&(n={effect:n});var a,r=n?n===!0||"number"==typeof n?i:n.effect||i:e;n=n||{},"number"==typeof n&&(n={duration:n}),a=!t.isEmptyObject(n),n.complete=o,n.delay&&s.delay(n.delay),a&&t.effects&&t.effects.effect[r]?s[e](n):r!==e&&s[r]?s[r](n.duration,n.easing,o):s.queue(function(i){t(this)[e](),o&&o.call(s[0]),i()})}}),t.widget,t.extend(t.expr[":"],{data:t.expr.createPseudo?t.expr.createPseudo(function(e){return function(i){return!!t.data(i,e)}}):function(e,i,s){return!!t.data(e,s[3])}}),t.fn.scrollParent=function(e){var i=this.css("position"),s="absolute"===i,n=e?/(auto|scroll|hidden)/:/(auto|scroll)/,o=this.parents().filter(function(){var e=t(this);return s&&"static"===e.css("position")?!1:n.test(e.css("overflow")+e.css("overflow-y")+e.css("overflow-x"))}).eq(0);return"fixed"!==i&&o.length?o:t(this[0].ownerDocument||document)},t.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());var s=!1;t(document).on("mouseup",function(){s=!1}),t.widget("ui.mouse",{version:"1.12.1",options:{cancel:"input, textarea, button, select, option",distance:1,delay:0},_mouseInit:function(){var e=this;this.element.on("mousedown."+this.widgetName,function(t){return e._mouseDown(t)}).on("click."+this.widgetName,function(i){return!0===t.data(i.target,e.widgetName+".preventClickEvent")?(t.removeData(i.target,e.widgetName+".preventClickEvent"),i.stopImmediatePropagation(),!1):void 0}),this.started=!1},_mouseDestroy:function(){this.element.off("."+this.widgetName),this._mouseMoveDelegate&&this.document.off("mousemove."+this.widgetName,this._mouseMoveDelegate).off("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(e){if(!s){this._mouseMoved=!1,this._mouseStarted&&this._mouseUp(e),this._mouseDownEvent=e;var i=this,n=1===e.which,o="string"==typeof this.options.cancel&&e.target.nodeName?t(e.target).closest(this.options.cancel).length:!1;return n&&!o&&this._mouseCapture(e)?(this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){i.mouseDelayMet=!0},this.options.delay)),this._mouseDistanceMet(e)&&this._mouseDelayMet(e)&&(this._mouseStarted=this._mouseStart(e)!==!1,!this._mouseStarted)?(e.preventDefault(),!0):(!0===t.data(e.target,this.widgetName+".preventClickEvent")&&t.removeData(e.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(t){return i._mouseMove(t)},this._mouseUpDelegate=function(t){return i._mouseUp(t)},this.document.on("mousemove."+this.widgetName,this._mouseMoveDelegate).on("mouseup."+this.widgetName,this._mouseUpDelegate),e.preventDefault(),s=!0,!0)):!0}},_mouseMove:function(e){if(this._mouseMoved){if(t.ui.ie&&(!document.documentMode||9>document.documentMode)&&!e.button)return this._mouseUp(e);if(!e.which)if(e.originalEvent.altKey||e.originalEvent.ctrlKey||e.originalEvent.metaKey||e.originalEvent.shiftKey)this.ignoreMissingWhich=!0;else if(!this.ignoreMissingWhich)return this._mouseUp(e)}return(e.which||e.button)&&(this._mouseMoved=!0),this._mouseStarted?(this._mouseDrag(e),e.preventDefault()):(this._mouseDistanceMet(e)&&this._mouseDelayMet(e)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,e)!==!1,this._mouseStarted?this._mouseDrag(e):this._mouseUp(e)),!this._mouseStarted)},_mouseUp:function(e){this.document.off("mousemove."+this.widgetName,this._mouseMoveDelegate).off("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,e.target===this._mouseDownEvent.target&&t.data(e.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(e)),this._mouseDelayTimer&&(clearTimeout(this._mouseDelayTimer),delete this._mouseDelayTimer),this.ignoreMissingWhich=!1,s=!1,e.preventDefault()},_mouseDistanceMet:function(t){return Math.max(Math.abs(this._mouseDownEvent.pageX-t.pageX),Math.abs(this._mouseDownEvent.pageY-t.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}}),t.widget("ui.sortable",t.ui.mouse,{version:"1.12.1",widgetEventPrefix:"sort",ready:!1,options:{appendTo:"parent",axis:!1,connectWith:!1,containment:!1,cursor:"auto",cursorAt:!1,dropOnEmpty:!0,forcePlaceholderSize:!1,forceHelperSize:!1,grid:!1,handle:!1,helper:"original",items:"> *",opacity:!1,placeholder:!1,revert:!1,scroll:!0,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1e3,activate:null,beforeStop:null,change:null,deactivate:null,out:null,over:null,receive:null,remove:null,sort:null,start:null,stop:null,update:null},_isOverAxis:function(t,e,i){return t>=e&&e+i>t},_isFloating:function(t){return/left|right/.test(t.css("float"))||/inline|table-cell/.test(t.css("display"))},_create:function(){this.containerCache={},this._addClass("ui-sortable"),this.refresh(),this.offset=this.element.offset(),this._mouseInit(),this._setHandleClassName(),this.ready=!0},_setOption:function(t,e){this._super(t,e),"handle"===t&&this._setHandleClassName()},_setHandleClassName:function(){var e=this;this._removeClass(this.element.find(".ui-sortable-handle"),"ui-sortable-handle"),t.each(this.items,function(){e._addClass(this.instance.options.handle?this.item.find(this.instance.options.handle):this.item,"ui-sortable-handle")})},_destroy:function(){this._mouseDestroy();for(var t=this.items.length-1;t>=0;t--)this.items[t].item.removeData(this.widgetName+"-item");return this},_mouseCapture:function(e,i){var s=null,n=!1,o=this;return this.reverting?!1:this.options.disabled||"static"===this.options.type?!1:(this._refreshItems(e),t(e.target).parents().each(function(){return t.data(this,o.widgetName+"-item")===o?(s=t(this),!1):void 0}),t.data(e.target,o.widgetName+"-item")===o&&(s=t(e.target)),s?!this.options.handle||i||(t(this.options.handle,s).find("*").addBack().each(function(){this===e.target&&(n=!0)}),n)?(this.currentItem=s,this._removeCurrentsFromItems(),!0):!1:!1)},_mouseStart:function(e,i,s){var n,o,a=this.options;if(this.currentContainer=this,this.refreshPositions(),this.helper=this._createHelper(e),this._cacheHelperProportions(),this._cacheMargins(),this.scrollParent=this.helper.scrollParent(),this.offset=this.currentItem.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},t.extend(this.offset,{click:{left:e.pageX-this.offset.left,top:e.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.helper.css("position","absolute"),this.cssPosition=this.helper.css("position"),this.originalPosition=this._generatePosition(e),this.originalPageX=e.pageX,this.originalPageY=e.pageY,a.cursorAt&&this._adjustOffsetFromHelper(a.cursorAt),this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]},this.helper[0]!==this.currentItem[0]&&this.currentItem.hide(),this._createPlaceholder(),a.containment&&this._setContainment(),a.cursor&&"auto"!==a.cursor&&(o=this.document.find("body"),this.storedCursor=o.css("cursor"),o.css("cursor",a.cursor),this.storedStylesheet=t("<style>*{ cursor: "+a.cursor+" !important; }</style>").appendTo(o)),a.opacity&&(this.helper.css("opacity")&&(this._storedOpacity=this.helper.css("opacity")),this.helper.css("opacity",a.opacity)),a.zIndex&&(this.helper.css("zIndex")&&(this._storedZIndex=this.helper.css("zIndex")),this.helper.css("zIndex",a.zIndex)),this.scrollParent[0]!==this.document[0]&&"HTML"!==this.scrollParent[0].tagName&&(this.overflowOffset=this.scrollParent.offset()),this._trigger("start",e,this._uiHash()),this._preserveHelperProportions||this._cacheHelperProportions(),!s)for(n=this.containers.length-1;n>=0;n--)this.containers[n]._trigger("activate",e,this._uiHash(this));return t.ui.ddmanager&&(t.ui.ddmanager.current=this),t.ui.ddmanager&&!a.dropBehaviour&&t.ui.ddmanager.prepareOffsets(this,e),this.dragging=!0,this._addClass(this.helper,"ui-sortable-helper"),this._mouseDrag(e),!0},_mouseDrag:function(e){var i,s,n,o,a=this.options,r=!1;for(this.position=this._generatePosition(e),this.positionAbs=this._convertPositionTo("absolute"),this.lastPositionAbs||(this.lastPositionAbs=this.positionAbs),this.options.scroll&&(this.scrollParent[0]!==this.document[0]&&"HTML"!==this.scrollParent[0].tagName?(this.overflowOffset.top+this.scrollParent[0].offsetHeight-e.pageY<a.scrollSensitivity?this.scrollParent[0].scrollTop=r=this.scrollParent[0].scrollTop+a.scrollSpeed:e.pageY-this.overflowOffset.top<a.scrollSensitivity&&(this.scrollParent[0].scrollTop=r=this.scrollParent[0].scrollTop-a.scrollSpeed),this.overflowOffset.left+this.scrollParent[0].offsetWidth-e.pageX<a.scrollSensitivity?this.scrollParent[0].scrollLeft=r=this.scrollParent[0].scrollLeft+a.scrollSpeed:e.pageX-this.overflowOffset.left<a.scrollSensitivity&&(this.scrollParent[0].scrollLeft=r=this.scrollParent[0].scrollLeft-a.scrollSpeed)):(e.pageY-this.document.scrollTop()<a.scrollSensitivity?r=this.document.scrollTop(this.document.scrollTop()-a.scrollSpeed):this.window.height()-(e.pageY-this.document.scrollTop())<a.scrollSensitivity&&(r=this.document.scrollTop(this.document.scrollTop()+a.scrollSpeed)),e.pageX-this.document.scrollLeft()<a.scrollSensitivity?r=this.document.scrollLeft(this.document.scrollLeft()-a.scrollSpeed):this.window.width()-(e.pageX-this.document.scrollLeft())<a.scrollSensitivity&&(r=this.document.scrollLeft(this.document.scrollLeft()+a.scrollSpeed))),r!==!1&&t.ui.ddmanager&&!a.dropBehaviour&&t.ui.ddmanager.prepareOffsets(this,e)),this.positionAbs=this._convertPositionTo("absolute"),this.options.axis&&"y"===this.options.axis||(this.helper[0].style.left=this.position.left+"px"),this.options.axis&&"x"===this.options.axis||(this.helper[0].style.top=this.position.top+"px"),i=this.items.length-1;i>=0;i--)if(s=this.items[i],n=s.item[0],o=this._intersectsWithPointer(s),o&&s.instance===this.currentContainer&&n!==this.currentItem[0]&&this.placeholder[1===o?"next":"prev"]()[0]!==n&&!t.contains(this.placeholder[0],n)&&("semi-dynamic"===this.options.type?!t.contains(this.element[0],n):!0)){if(this.direction=1===o?"down":"up","pointer"!==this.options.tolerance&&!this._intersectsWithSides(s))break;this._rearrange(e,s),this._trigger("change",e,this._uiHash());break}return this._contactContainers(e),t.ui.ddmanager&&t.ui.ddmanager.drag(this,e),this._trigger("sort",e,this._uiHash()),this.lastPositionAbs=this.positionAbs,!1},_mouseStop:function(e,i){if(e){if(t.ui.ddmanager&&!this.options.dropBehaviour&&t.ui.ddmanager.drop(this,e),this.options.revert){var s=this,n=this.placeholder.offset(),o=this.options.axis,a={};o&&"x"!==o||(a.left=n.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]===this.document[0].body?0:this.offsetParent[0].scrollLeft)),o&&"y"!==o||(a.top=n.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]===this.document[0].body?0:this.offsetParent[0].scrollTop)),this.reverting=!0,t(this.helper).animate(a,parseInt(this.options.revert,10)||500,function(){s._clear(e)})}else this._clear(e,i);return!1}},cancel:function(){if(this.dragging){this._mouseUp(new t.Event("mouseup",{target:null})),"original"===this.options.helper?(this.currentItem.css(this._storedCSS),this._removeClass(this.currentItem,"ui-sortable-helper")):this.currentItem.show();for(var e=this.containers.length-1;e>=0;e--)this.containers[e]._trigger("deactivate",null,this._uiHash(this)),this.containers[e].containerCache.over&&(this.containers[e]._trigger("out",null,this._uiHash(this)),this.containers[e].containerCache.over=0)}return this.placeholder&&(this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]),"original"!==this.options.helper&&this.helper&&this.helper[0].parentNode&&this.helper.remove(),t.extend(this,{helper:null,dragging:!1,reverting:!1,_noFinalSort:null}),this.domPosition.prev?t(this.domPosition.prev).after(this.currentItem):t(this.domPosition.parent).prepend(this.currentItem)),this},serialize:function(e){var i=this._getItemsAsjQuery(e&&e.connected),s=[];return e=e||{},t(i).each(function(){var i=(t(e.item||this).attr(e.attribute||"id")||"").match(e.expression||/(.+)[\-=_](.+)/);i&&s.push((e.key||i[1]+"[]")+"="+(e.key&&e.expression?i[1]:i[2]))}),!s.length&&e.key&&s.push(e.key+"="),s.join("&")},toArray:function(e){var i=this._getItemsAsjQuery(e&&e.connected),s=[];return e=e||{},i.each(function(){s.push(t(e.item||this).attr(e.attribute||"id")||"")}),s},_intersectsWith:function(t){var e=this.positionAbs.left,i=e+this.helperProportions.width,s=this.positionAbs.top,n=s+this.helperProportions.height,o=t.left,a=o+t.width,r=t.top,l=r+t.height,h=this.offset.click.top,c=this.offset.click.left,u="x"===this.options.axis||s+h>r&&l>s+h,d="y"===this.options.axis||e+c>o&&a>e+c,p=u&&d;return"pointer"===this.options.tolerance||this.options.forcePointerForContainers||"pointer"!==this.options.tolerance&&this.helperProportions[this.floating?"width":"height"]>t[this.floating?"width":"height"]?p:e+this.helperProportions.width/2>o&&a>i-this.helperProportions.width/2&&s+this.helperProportions.height/2>r&&l>n-this.helperProportions.height/2},_intersectsWithPointer:function(t){var e,i,s="x"===this.options.axis||this._isOverAxis(this.positionAbs.top+this.offset.click.top,t.top,t.height),n="y"===this.options.axis||this._isOverAxis(this.positionAbs.left+this.offset.click.left,t.left,t.width),o=s&&n;return o?(e=this._getDragVerticalDirection(),i=this._getDragHorizontalDirection(),this.floating?"right"===i||"down"===e?2:1:e&&("down"===e?2:1)):!1},_intersectsWithSides:function(t){var e=this._isOverAxis(this.positionAbs.top+this.offset.click.top,t.top+t.height/2,t.height),i=this._isOverAxis(this.positionAbs.left+this.offset.click.left,t.left+t.width/2,t.width),s=this._getDragVerticalDirection(),n=this._getDragHorizontalDirection();return this.floating&&n?"right"===n&&i||"left"===n&&!i:s&&("down"===s&&e||"up"===s&&!e)},_getDragVerticalDirection:function(){var t=this.positionAbs.top-this.lastPositionAbs.top;return 0!==t&&(t>0?"down":"up")},_getDragHorizontalDirection:function(){var t=this.positionAbs.left-this.lastPositionAbs.left;return 0!==t&&(t>0?"right":"left")},refresh:function(t){return this._refreshItems(t),this._setHandleClassName(),this.refreshPositions(),this},_connectWith:function(){var t=this.options;return t.connectWith.constructor===String?[t.connectWith]:t.connectWith},_getItemsAsjQuery:function(e){function i(){r.push(this)}var s,n,o,a,r=[],l=[],h=this._connectWith();if(h&&e)for(s=h.length-1;s>=0;s--)for(o=t(h[s],this.document[0]),n=o.length-1;n>=0;n--)a=t.data(o[n],this.widgetFullName),a&&a!==this&&!a.options.disabled&&l.push([t.isFunction(a.options.items)?a.options.items.call(a.element):t(a.options.items,a.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),a]);for(l.push([t.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):t(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]),s=l.length-1;s>=0;s--)l[s][0].each(i);return t(r)},_removeCurrentsFromItems:function(){var e=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=t.grep(this.items,function(t){for(var i=0;e.length>i;i++)if(e[i]===t.item[0])return!1;return!0})},_refreshItems:function(e){this.items=[],this.containers=[this];var i,s,n,o,a,r,l,h,c=this.items,u=[[t.isFunction(this.options.items)?this.options.items.call(this.element[0],e,{item:this.currentItem}):t(this.options.items,this.element),this]],d=this._connectWith();if(d&&this.ready)for(i=d.length-1;i>=0;i--)for(n=t(d[i],this.document[0]),s=n.length-1;s>=0;s--)o=t.data(n[s],this.widgetFullName),o&&o!==this&&!o.options.disabled&&(u.push([t.isFunction(o.options.items)?o.options.items.call(o.element[0],e,{item:this.currentItem}):t(o.options.items,o.element),o]),this.containers.push(o));for(i=u.length-1;i>=0;i--)for(a=u[i][1],r=u[i][0],s=0,h=r.length;h>s;s++)l=t(r[s]),l.data(this.widgetName+"-item",a),c.push({item:l,instance:a,width:0,height:0,left:0,top:0})},refreshPositions:function(e){this.floating=this.items.length?"x"===this.options.axis||this._isFloating(this.items[0].item):!1,this.offsetParent&&this.helper&&(this.offset.parent=this._getParentOffset());var i,s,n,o;for(i=this.items.length-1;i>=0;i--)s=this.items[i],s.instance!==this.currentContainer&&this.currentContainer&&s.item[0]!==this.currentItem[0]||(n=this.options.toleranceElement?t(this.options.toleranceElement,s.item):s.item,e||(s.width=n.outerWidth(),s.height=n.outerHeight()),o=n.offset(),s.left=o.left,s.top=o.top);if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(i=this.containers.length-1;i>=0;i--)o=this.containers[i].element.offset(),this.containers[i].containerCache.left=o.left,this.containers[i].containerCache.top=o.top,this.containers[i].containerCache.width=this.containers[i].element.outerWidth(),this.containers[i].containerCache.height=this.containers[i].element.outerHeight();return this},_createPlaceholder:function(e){e=e||this;var i,s=e.options;s.placeholder&&s.placeholder.constructor!==String||(i=s.placeholder,s.placeholder={element:function(){var s=e.currentItem[0].nodeName.toLowerCase(),n=t("<"+s+">",e.document[0]);return e._addClass(n,"ui-sortable-placeholder",i||e.currentItem[0].className)._removeClass(n,"ui-sortable-helper"),"tbody"===s?e._createTrPlaceholder(e.currentItem.find("tr").eq(0),t("<tr>",e.document[0]).appendTo(n)):"tr"===s?e._createTrPlaceholder(e.currentItem,n):"img"===s&&n.attr("src",e.currentItem.attr("src")),i||n.css("visibility","hidden"),n},update:function(t,n){(!i||s.forcePlaceholderSize)&&(n.height()||n.height(e.currentItem.innerHeight()-parseInt(e.currentItem.css("paddingTop")||0,10)-parseInt(e.currentItem.css("paddingBottom")||0,10)),n.width()||n.width(e.currentItem.innerWidth()-parseInt(e.currentItem.css("paddingLeft")||0,10)-parseInt(e.currentItem.css("paddingRight")||0,10)))}}),e.placeholder=t(s.placeholder.element.call(e.element,e.currentItem)),e.currentItem.after(e.placeholder),s.placeholder.update(e,e.placeholder)},_createTrPlaceholder:function(e,i){var s=this;e.children().each(function(){t("<td>&#160;</td>",s.document[0]).attr("colspan",t(this).attr("colspan")||1).appendTo(i)})},_contactContainers:function(e){var i,s,n,o,a,r,l,h,c,u,d=null,p=null;for(i=this.containers.length-1;i>=0;i--)if(!t.contains(this.currentItem[0],this.containers[i].element[0]))if(this._intersectsWith(this.containers[i].containerCache)){if(d&&t.contains(this.containers[i].element[0],d.element[0]))continue;d=this.containers[i],p=i}else this.containers[i].containerCache.over&&(this.containers[i]._trigger("out",e,this._uiHash(this)),this.containers[i].containerCache.over=0);if(d)if(1===this.containers.length)this.containers[p].containerCache.over||(this.containers[p]._trigger("over",e,this._uiHash(this)),this.containers[p].containerCache.over=1);else{for(n=1e4,o=null,c=d.floating||this._isFloating(this.currentItem),a=c?"left":"top",r=c?"width":"height",u=c?"pageX":"pageY",s=this.items.length-1;s>=0;s--)t.contains(this.containers[p].element[0],this.items[s].item[0])&&this.items[s].item[0]!==this.currentItem[0]&&(l=this.items[s].item.offset()[a],h=!1,e[u]-l>this.items[s][r]/2&&(h=!0),n>Math.abs(e[u]-l)&&(n=Math.abs(e[u]-l),o=this.items[s],this.direction=h?"up":"down"));if(!o&&!this.options.dropOnEmpty)return;if(this.currentContainer===this.containers[p])return this.currentContainer.containerCache.over||(this.containers[p]._trigger("over",e,this._uiHash()),this.currentContainer.containerCache.over=1),void 0;o?this._rearrange(e,o,null,!0):this._rearrange(e,null,this.containers[p].element,!0),this._trigger("change",e,this._uiHash()),this.containers[p]._trigger("change",e,this._uiHash(this)),this.currentContainer=this.containers[p],this.options.placeholder.update(this.currentContainer,this.placeholder),this.containers[p]._trigger("over",e,this._uiHash(this)),this.containers[p].containerCache.over=1}},_createHelper:function(e){var i=this.options,s=t.isFunction(i.helper)?t(i.helper.apply(this.element[0],[e,this.currentItem])):"clone"===i.helper?this.currentItem.clone():this.currentItem;return s.parents("body").length||t("parent"!==i.appendTo?i.appendTo:this.currentItem[0].parentNode)[0].appendChild(s[0]),s[0]===this.currentItem[0]&&(this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")}),(!s[0].style.width||i.forceHelperSize)&&s.width(this.currentItem.width()),(!s[0].style.height||i.forceHelperSize)&&s.height(this.currentItem.height()),s},_adjustOffsetFromHelper:function(e){"string"==typeof e&&(e=e.split(" ")),t.isArray(e)&&(e={left:+e[0],top:+e[1]||0}),"left"in e&&(this.offset.click.left=e.left+this.margins.left),"right"in e&&(this.offset.click.left=this.helperProportions.width-e.right+this.margins.left),"top"in e&&(this.offset.click.top=e.top+this.margins.top),"bottom"in e&&(this.offset.click.top=this.helperProportions.height-e.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var e=this.offsetParent.offset();return"absolute"===this.cssPosition&&this.scrollParent[0]!==this.document[0]&&t.contains(this.scrollParent[0],this.offsetParent[0])&&(e.left+=this.scrollParent.scrollLeft(),e.top+=this.scrollParent.scrollTop()),(this.offsetParent[0]===this.document[0].body||this.offsetParent[0].tagName&&"html"===this.offsetParent[0].tagName.toLowerCase()&&t.ui.ie)&&(e={top:0,left:0}),{top:e.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:e.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"===this.cssPosition){var t=this.currentItem.position();return{top:t.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:t.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var e,i,s,n=this.options;"parent"===n.containment&&(n.containment=this.helper[0].parentNode),("document"===n.containment||"window"===n.containment)&&(this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,"document"===n.containment?this.document.width():this.window.width()-this.helperProportions.width-this.margins.left,("document"===n.containment?this.document.height()||document.body.parentNode.scrollHeight:this.window.height()||this.document[0].body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]),/^(document|window|parent)$/.test(n.containment)||(e=t(n.containment)[0],i=t(n.containment).offset(),s="hidden"!==t(e).css("overflow"),this.containment=[i.left+(parseInt(t(e).css("borderLeftWidth"),10)||0)+(parseInt(t(e).css("paddingLeft"),10)||0)-this.margins.left,i.top+(parseInt(t(e).css("borderTopWidth"),10)||0)+(parseInt(t(e).css("paddingTop"),10)||0)-this.margins.top,i.left+(s?Math.max(e.scrollWidth,e.offsetWidth):e.offsetWidth)-(parseInt(t(e).css("borderLeftWidth"),10)||0)-(parseInt(t(e).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,i.top+(s?Math.max(e.scrollHeight,e.offsetHeight):e.offsetHeight)-(parseInt(t(e).css("borderTopWidth"),10)||0)-(parseInt(t(e).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top])},_convertPositionTo:function(e,i){i||(i=this.position);var s="absolute"===e?1:-1,n="absolute"!==this.cssPosition||this.scrollParent[0]!==this.document[0]&&t.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,o=/(html|body)/i.test(n[0].tagName);return{top:i.top+this.offset.relative.top*s+this.offset.parent.top*s-("fixed"===this.cssPosition?-this.scrollParent.scrollTop():o?0:n.scrollTop())*s,left:i.left+this.offset.relative.left*s+this.offset.parent.left*s-("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():o?0:n.scrollLeft())*s}
},_generatePosition:function(e){var i,s,n=this.options,o=e.pageX,a=e.pageY,r="absolute"!==this.cssPosition||this.scrollParent[0]!==this.document[0]&&t.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,l=/(html|body)/i.test(r[0].tagName);return"relative"!==this.cssPosition||this.scrollParent[0]!==this.document[0]&&this.scrollParent[0]!==this.offsetParent[0]||(this.offset.relative=this._getRelativeOffset()),this.originalPosition&&(this.containment&&(e.pageX-this.offset.click.left<this.containment[0]&&(o=this.containment[0]+this.offset.click.left),e.pageY-this.offset.click.top<this.containment[1]&&(a=this.containment[1]+this.offset.click.top),e.pageX-this.offset.click.left>this.containment[2]&&(o=this.containment[2]+this.offset.click.left),e.pageY-this.offset.click.top>this.containment[3]&&(a=this.containment[3]+this.offset.click.top)),n.grid&&(i=this.originalPageY+Math.round((a-this.originalPageY)/n.grid[1])*n.grid[1],a=this.containment?i-this.offset.click.top>=this.containment[1]&&i-this.offset.click.top<=this.containment[3]?i:i-this.offset.click.top>=this.containment[1]?i-n.grid[1]:i+n.grid[1]:i,s=this.originalPageX+Math.round((o-this.originalPageX)/n.grid[0])*n.grid[0],o=this.containment?s-this.offset.click.left>=this.containment[0]&&s-this.offset.click.left<=this.containment[2]?s:s-this.offset.click.left>=this.containment[0]?s-n.grid[0]:s+n.grid[0]:s)),{top:a-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.scrollParent.scrollTop():l?0:r.scrollTop()),left:o-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():l?0:r.scrollLeft())}},_rearrange:function(t,e,i,s){i?i[0].appendChild(this.placeholder[0]):e.item[0].parentNode.insertBefore(this.placeholder[0],"down"===this.direction?e.item[0]:e.item[0].nextSibling),this.counter=this.counter?++this.counter:1;var n=this.counter;this._delay(function(){n===this.counter&&this.refreshPositions(!s)})},_clear:function(t,e){function i(t,e,i){return function(s){i._trigger(t,s,e._uiHash(e))}}this.reverting=!1;var s,n=[];if(!this._noFinalSort&&this.currentItem.parent().length&&this.placeholder.before(this.currentItem),this._noFinalSort=null,this.helper[0]===this.currentItem[0]){for(s in this._storedCSS)("auto"===this._storedCSS[s]||"static"===this._storedCSS[s])&&(this._storedCSS[s]="");this.currentItem.css(this._storedCSS),this._removeClass(this.currentItem,"ui-sortable-helper")}else this.currentItem.show();for(this.fromOutside&&!e&&n.push(function(t){this._trigger("receive",t,this._uiHash(this.fromOutside))}),!this.fromOutside&&this.domPosition.prev===this.currentItem.prev().not(".ui-sortable-helper")[0]&&this.domPosition.parent===this.currentItem.parent()[0]||e||n.push(function(t){this._trigger("update",t,this._uiHash())}),this!==this.currentContainer&&(e||(n.push(function(t){this._trigger("remove",t,this._uiHash())}),n.push(function(t){return function(e){t._trigger("receive",e,this._uiHash(this))}}.call(this,this.currentContainer)),n.push(function(t){return function(e){t._trigger("update",e,this._uiHash(this))}}.call(this,this.currentContainer)))),s=this.containers.length-1;s>=0;s--)e||n.push(i("deactivate",this,this.containers[s])),this.containers[s].containerCache.over&&(n.push(i("out",this,this.containers[s])),this.containers[s].containerCache.over=0);if(this.storedCursor&&(this.document.find("body").css("cursor",this.storedCursor),this.storedStylesheet.remove()),this._storedOpacity&&this.helper.css("opacity",this._storedOpacity),this._storedZIndex&&this.helper.css("zIndex","auto"===this._storedZIndex?"":this._storedZIndex),this.dragging=!1,e||this._trigger("beforeStop",t,this._uiHash()),this.placeholder[0].parentNode.removeChild(this.placeholder[0]),this.cancelHelperRemoval||(this.helper[0]!==this.currentItem[0]&&this.helper.remove(),this.helper=null),!e){for(s=0;n.length>s;s++)n[s].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!this.cancelHelperRemoval},_trigger:function(){t.Widget.prototype._trigger.apply(this,arguments)===!1&&this.cancel()},_uiHash:function(e){var i=e||this;return{helper:i.helper,placeholder:i.placeholder||t([]),position:i.position,originalPosition:i.originalPosition,offset:i.positionAbs,item:i.currentItem,sender:e?e.element:null}}})});

/*!
 * jQuery Browser Plugin 0.1.0
 * https://github.com/gabceb/jquery-browser-plugin
 *
 * Original jquery-browser code Copyright 2005, 2015 jQuery Foundation, Inc. and other contributors
 * http://jquery.org/license
 *
 * Modifications Copyright 2015 Gabriel Cebrian
 * https://github.com/gabceb
 *
 * Released under the MIT license
 *
 * Date: 23-11-2015
 */!function(a){"function"==typeof define&&define.amd?define(["jquery"],function(b){return a(b)}):"object"==typeof module&&"object"==typeof module.exports?module.exports=a(require("jquery")):a(window.jQuery)}(function(a){"use strict";function b(a){void 0===a&&(a=window.navigator.userAgent),a=a.toLowerCase();var b=/(edge)\/([\w.]+)/.exec(a)||/(opr)[\/]([\w.]+)/.exec(a)||/(chrome)[ \/]([\w.]+)/.exec(a)||/(iemobile)[\/]([\w.]+)/.exec(a)||/(version)(applewebkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(a)||/(webkit)[ \/]([\w.]+).*(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(a)||/(webkit)[ \/]([\w.]+)/.exec(a)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a)||/(msie) ([\w.]+)/.exec(a)||a.indexOf("trident")>=0&&/(rv)(?::| )([\w.]+)/.exec(a)||a.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a)||[],c=/(ipad)/.exec(a)||/(ipod)/.exec(a)||/(windows phone)/.exec(a)||/(iphone)/.exec(a)||/(kindle)/.exec(a)||/(silk)/.exec(a)||/(android)/.exec(a)||/(win)/.exec(a)||/(mac)/.exec(a)||/(linux)/.exec(a)||/(cros)/.exec(a)||/(playbook)/.exec(a)||/(bb)/.exec(a)||/(blackberry)/.exec(a)||[],d={},e={browser:b[5]||b[3]||b[1]||"",version:b[2]||b[4]||"0",versionNumber:b[4]||b[2]||"0",platform:c[0]||""};if(e.browser&&(d[e.browser]=!0,d.version=e.version,d.versionNumber=parseInt(e.versionNumber,10)),e.platform&&(d[e.platform]=!0),(d.android||d.bb||d.blackberry||d.ipad||d.iphone||d.ipod||d.kindle||d.playbook||d.silk||d["windows phone"])&&(d.mobile=!0),(d.cros||d.mac||d.linux||d.win)&&(d.desktop=!0),(d.chrome||d.opr||d.safari)&&(d.webkit=!0),d.rv||d.iemobile){var f="msie";e.browser=f,d[f]=!0}if(d.edge){delete d.edge;var g="msedge";e.browser=g,d[g]=!0}if(d.safari&&d.blackberry){var h="blackberry";e.browser=h,d[h]=!0}if(d.safari&&d.playbook){var i="playbook";e.browser=i,d[i]=!0}if(d.bb){var j="blackberry";e.browser=j,d[j]=!0}if(d.opr){var k="opera";e.browser=k,d[k]=!0}if(d.safari&&d.android){var l="android";e.browser=l,d[l]=!0}if(d.safari&&d.kindle){var m="kindle";e.browser=m,d[m]=!0}if(d.safari&&d.silk){var n="silk";e.browser=n,d[n]=!0}return d.name=e.browser,d.platform=e.platform,d}return window.jQBrowser=b(window.navigator.userAgent),window.jQBrowser.uaMatch=b,a&&(a.browser=window.jQBrowser),window.jQBrowser});

/*
 * jQuery BBQ: Back Button & Query Library - v1.3pre - 8/26/2010
 * http://benalman.com/projects/jquery-bbq-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,r){var h,n=Array.prototype.slice,t=decodeURIComponent,a=$.param,j,c,m,y,b=$.bbq=$.bbq||{},s,x,k,e=$.event.special,d="hashchange",B="querystring",F="fragment",z="elemUrlAttr",l="href",w="src",p=/^.*\?|#.*$/g,u,H,g,i,C,E={};function G(I){return typeof I==="string"}function D(J){var I=n.call(arguments,1);return function(){return J.apply(this,I.concat(n.call(arguments)))}}function o(I){return I.replace(H,"$2")}function q(I){return I.replace(/(?:^[^?#]*\?([^#]*).*$)?.*/,"$1")}function f(K,P,I,L,J){var R,O,N,Q,M;if(L!==h){N=I.match(K?H:/^([^#?]*)\??([^#]*)(#?.*)/);M=N[3]||"";if(J===2&&G(L)){O=L.replace(K?u:p,"")}else{Q=m(N[2]);L=G(L)?m[K?F:B](L):L;O=J===2?L:J===1?$.extend({},L,Q):$.extend({},Q,L);O=j(O);if(K){O=O.replace(g,t)}}R=N[1]+(K?C:O||!N[1]?"?":"")+O+M}else{R=P(I!==h?I:location.href)}return R}a[B]=D(f,0,q);a[F]=c=D(f,1,o);a.sorted=j=function(J,K){var I=[],L={};$.each(a(J,K).split("&"),function(P,M){var O=M.replace(/(?:%5B|=).*$/,""),N=L[O];if(!N){N=L[O]=[];I.push(O)}N.push(M)});return $.map(I.sort(),function(M){return L[M]}).join("&")};c.noEscape=function(J){J=J||"";var I=$.map(J.split(""),encodeURIComponent);g=new RegExp(I.join("|"),"g")};c.noEscape(",/");c.ajaxCrawlable=function(I){if(I!==h){if(I){u=/^.*(?:#!|#)/;H=/^([^#]*)(?:#!|#)?(.*)$/;C="#!"}else{u=/^.*#/;H=/^([^#]*)#?(.*)$/;C="#"}i=!!I}return i};c.ajaxCrawlable(0);$.deparam=m=function(L,I){var K={},J={"true":!0,"false":!1,"null":null};$.each(L.replace(/\+/g," ").split("&"),function(O,T){var N=T.split("="),S=t(N[0]),M,R=K,P=0,U=S.split("]["),Q=U.length-1;if(/\[/.test(U[0])&&/\]$/.test(U[Q])){U[Q]=U[Q].replace(/\]$/,"");U=U.shift().split("[").concat(U);Q=U.length-1}else{Q=0}if(N.length===2){M=t(N[1]);if(I){M=M&&!isNaN(M)?+M:M==="undefined"?h:J[M]!==h?J[M]:M}if(Q){for(;P<=Q;P++){S=U[P]===""?R.length:U[P];R=R[S]=P<Q?R[S]||(U[P+1]&&isNaN(U[P+1])?{}:[]):M}}else{if($.isArray(K[S])){K[S].push(M)}else{if(K[S]!==h){K[S]=[K[S],M]}else{K[S]=M}}}}else{if(S){K[S]=I?h:""}}});return K};function A(K,I,J){if(I===h||typeof I==="boolean"){J=I;I=a[K?F:B]()}else{I=G(I)?I.replace(K?u:p,""):I}return m(I,J)}m[B]=D(A,0);m[F]=y=D(A,1);$[z]||($[z]=function(I){return $.extend(E,I)})({a:l,base:l,iframe:w,img:w,input:w,form:"action",link:l,script:w});k=$[z];function v(L,J,K,I){if(!G(K)&&typeof K!=="object"){I=K;K=J;J=h}return this.each(function(){var O=$(this),M=J||k()[(this.nodeName||"").toLowerCase()]||"",N=M&&O.attr(M)||"";O.attr(M,a[L](N,K,I))})}$.fn[B]=D(v,B);$.fn[F]=D(v,F);b.pushState=s=function(L,I){if(G(L)&&/^#/.test(L)&&I===h){I=2}var K=L!==h,J=c(location.href,K?L:{},K?I:2);location.href=J};b.getState=x=function(I,J){return I===h||typeof I==="boolean"?y(I):y(J)[I]};b.removeState=function(I){var J={};if(I!==h){J=x();$.each($.isArray(I)?I:arguments,function(L,K){delete J[K]})}s(J,2)};e[d]=$.extend(e[d],{add:function(I){var K;function J(M){var L=M[F]=c();M.getState=function(N,O){return N===h||typeof N==="boolean"?m(L,N):m(L,O)[N]};K.apply(this,arguments)}if($.isFunction(I)){K=I;return J}else{K=I.handler;I.handler=J}}})})(jQuery,this);
/*
 * jQuery hashchange event - v1.3 - 7/21/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,e,b){var c="hashchange",h=document,f,g=$.event.special,i=h.documentMode,d="on"+c in e&&(i===b||i>7);function a(j){j=j||location.href;return"#"+j.replace(/^[^#]*#?(.*)$/,"$1")}$.fn[c]=function(j){return j?this.bind(c,j):this.trigger(c)};$.fn[c].delay=50;g[c]=$.extend(g[c],{setup:function(){if(d){return false}$(f.start)},teardown:function(){if(d){return false}$(f.stop)}});f=(function(){var j={},p,m=a(),k=function(q){return q},l=k,o=k;j.start=function(){p||n()};j.stop=function(){p&&clearTimeout(p);p=b};function n(){var r=a(),q=o(m);if(r!==m){l(m=r,q);$(e).trigger(c)}else{if(q!==m){location.href=location.href.replace(/#.*/,"")+q}}p=setTimeout(n,$.fn[c].delay)}$.browser.msie&&!d&&(function(){var q,r;j.start=function(){if(!q){r=$.fn[c].src;r=r&&r+a();q=$('<iframe tabindex="-1" title="empty"/>').hide().one("load",function(){r||l(a());n()}).attr("src",r||"javascript:0").insertAfter("body")[0].contentWindow;h.onpropertychange=function(){try{if(event.propertyName==="title"){q.document.title=h.title}}catch(s){}}}};j.stop=k;o=function(){return a(q.location.href)};l=function(v,s){var u=q.document,t=$.fn[c].domain;if(v!==s){u.title=h.title;u.open();t&&u.write('<script>document.domain="'+t+'"<\/script>');u.close();q.location.hash=v}}})();return j})()})(jQuery,this);

/*! Lazy Load 1.9.7 - MIT license - Copyright 2010-2015 Mika Tuupola */
!function(a,b,c,d){var e=a(b);a.fn.lazyload=function(f){function g(){var b=0;i.each(function(){var c=a(this);if(!j.skip_invisible||c.is(":visible"))if(a.abovethetop(this,j)||a.leftofbegin(this,j));else if(a.belowthefold(this,j)||a.rightoffold(this,j)){if(++b>j.failure_limit)return!1}else c.trigger("appear"),b=0})}var h,i=this,j={threshold:0,failure_limit:0,event:"scroll",effect:"show",container:b,data_attribute:"original",skip_invisible:!1,appear:null,load:null,placeholder:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"};return f&&(d!==f.failurelimit&&(f.failure_limit=f.failurelimit,delete f.failurelimit),d!==f.effectspeed&&(f.effect_speed=f.effectspeed,delete f.effectspeed),a.extend(j,f)),h=j.container===d||j.container===b?e:a(j.container),0===j.event.indexOf("scroll")&&h.bind(j.event,function(){return g()}),this.each(function(){var b=this,c=a(b);b.loaded=!1,(c.attr("src")===d||c.attr("src")===!1)&&c.is("img")&&c.attr("src",j.placeholder),c.one("appear",function(){if(!this.loaded){if(j.appear){var d=i.length;j.appear.call(b,d,j)}a("<img />").bind("load",function(){var d=c.attr("data-"+j.data_attribute);c.hide(),c.is("img")?c.attr("src",d):c.css("background-image","url('"+d+"')"),c[j.effect](j.effect_speed),b.loaded=!0;var e=a.grep(i,function(a){return!a.loaded});if(i=a(e),j.load){var f=i.length;j.load.call(b,f,j)}}).attr("src",c.attr("data-"+j.data_attribute))}}),0!==j.event.indexOf("scroll")&&c.bind(j.event,function(){b.loaded||c.trigger("appear")})}),e.bind("resize",function(){g()}),/(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion)&&e.bind("pageshow",function(b){b.originalEvent&&b.originalEvent.persisted&&i.each(function(){a(this).trigger("appear")})}),a(c).ready(function(){g()}),this},a.belowthefold=function(c,f){var g;return g=f.container===d||f.container===b?(b.innerHeight?b.innerHeight:e.height())+e.scrollTop():a(f.container).offset().top+a(f.container).height(),g<=a(c).offset().top-f.threshold},a.rightoffold=function(c,f){var g;return g=f.container===d||f.container===b?e.width()+e.scrollLeft():a(f.container).offset().left+a(f.container).width(),g<=a(c).offset().left-f.threshold},a.abovethetop=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollTop():a(f.container).offset().top,g>=a(c).offset().top+f.threshold+a(c).height()},a.leftofbegin=function(c,f){var g;return g=f.container===d||f.container===b?e.scrollLeft():a(f.container).offset().left,g>=a(c).offset().left+f.threshold+a(c).width()},a.inviewport=function(b,c){return!(a.rightoffold(b,c)||a.leftofbegin(b,c)||a.belowthefold(b,c)||a.abovethetop(b,c))},a.extend(a.expr[":"],{"below-the-fold":function(b){return a.belowthefold(b,{threshold:0})},"above-the-top":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-screen":function(b){return a.rightoffold(b,{threshold:0})},"left-of-screen":function(b){return!a.rightoffold(b,{threshold:0})},"in-viewport":function(b){return a.inviewport(b,{threshold:0})},"above-the-fold":function(b){return!a.belowthefold(b,{threshold:0})},"right-of-fold":function(b){return a.rightoffold(b,{threshold:0})},"left-of-fold":function(b){return!a.rightoffold(b,{threshold:0})}})}(jQuery,window,document);

!function(e){"use strict";function t(t){var r=t.data;t.isDefaultPrevented()||(t.preventDefault(),e(this).ajaxSubmit(r))}function r(t){var r=t.target,a=e(r);if(!a.is("[type=submit],[type=image]")){var n=a.closest("[type=submit]");if(0===n.length)return;r=n[0]}var i=this;if(i.clk=r,"image"==r.type)if(void 0!==t.offsetX)i.clk_x=t.offsetX,i.clk_y=t.offsetY;else if("function"==typeof e.fn.offset){var o=a.offset();i.clk_x=t.pageX-o.left,i.clk_y=t.pageY-o.top}else i.clk_x=t.pageX-r.offsetLeft,i.clk_y=t.pageY-r.offsetTop;setTimeout(function(){i.clk=i.clk_x=i.clk_y=null},100)}function a(){if(e.fn.ajaxSubmit.debug){var t="[jquery.form] "+Array.prototype.join.call(arguments,"");window.console&&window.console.log?window.console.log(t):window.opera&&window.opera.postError&&window.opera.postError(t)}}var n={};n.fileapi=void 0!==e("<input type='file'/>").get(0).files,n.formdata=void 0!==window.FormData;var i=!!e.fn.prop;e.fn.attr2=function(e,t){if(!i)return this.attr.apply(this,arguments);var r=this.prop.apply(this,arguments);return r&&r.jquery||"string"==typeof r?r:this.attr.apply(this,arguments)},e.fn.ajaxSubmit=function(t){function r(t){var r,a,n=e.param(t).split("&"),i=n.length,o=[];for(r=0;r<i;r++)n[r]=n[r].replace(/\+/g," "),a=n[r].split("="),o.push([decodeURIComponent(a[0]),decodeURIComponent(a[1])]);return o}function o(r){function n(e){return e.contentWindow?e.contentWindow.document:e.contentDocument?e.contentDocument:e.document}function o(){function t(){try{var e=n(g).readyState;a("state = "+e),e&&"uninitialized"==e.toLowerCase()&&setTimeout(t,50)}catch(e){a("Server abort: ",e," (",e.name,")"),u(D),j&&clearTimeout(j),j=void 0}}var r=c.attr2("target"),i=c.attr2("action");w.setAttribute("target",h),s||w.setAttribute("method","POST"),i!=m.url&&w.setAttribute("action",m.url),m.skipEncodingOverride||s&&!/post/i.test(s)||c.attr({encoding:"multipart/form-data",enctype:"multipart/form-data"}),m.timeout&&(j=setTimeout(function(){T=!0,u(k)},m.timeout));var o=[];try{if(m.extraData)for(var l in m.extraData)m.extraData.hasOwnProperty(l)&&(e.isPlainObject(m.extraData[l])&&m.extraData[l].hasOwnProperty("name")&&m.extraData[l].hasOwnProperty("value")?o.push(e('<input type="hidden" name="'+m.extraData[l].name+'">').val(m.extraData[l].value).appendTo(w)[0]):o.push(e('<input type="hidden" name="'+l+'">').val(m.extraData[l]).appendTo(w)[0]));m.iframeTarget||(v.appendTo("body"),g.attachEvent?g.attachEvent("onload",u):g.addEventListener("load",u,!1)),setTimeout(t,15);try{w.submit()}catch(e){document.createElement("form").submit.apply(w)}}finally{w.setAttribute("action",i),r?w.setAttribute("target",r):c.removeAttr("target"),e(o).remove()}}function u(t){if(!x.aborted&&!F){try{M=n(g)}catch(e){a("cannot access response document: ",e),t=D}if(t===k&&x)return x.abort("timeout"),void S.reject(x,"timeout");if(t==D&&x)return x.abort("server abort"),void S.reject(x,"error","server abort");if(M&&M.location.href!=m.iframeSrc||T){g.detachEvent?g.detachEvent("onload",u):g.removeEventListener("load",u,!1);var r,i="success";try{if(T)throw"timeout";var o="xml"==m.dataType||M.XMLDocument||e.isXMLDoc(M);if(a("isXml="+o),!o&&window.opera&&(null===M.body||!M.body.innerHTML)&&--O)return a("requeing onLoad callback, DOM not available"),void setTimeout(u,250);var s=M.body?M.body:M.documentElement;x.responseText=s?s.innerHTML:null,x.responseXML=M.XMLDocument?M.XMLDocument:M,o&&(m.dataType="xml"),x.getResponseHeader=function(e){return{"content-type":m.dataType}[e]},s&&(x.status=Number(s.getAttribute("status"))||x.status,x.statusText=s.getAttribute("statusText")||x.statusText);var l=(m.dataType||"").toLowerCase(),c=/(json|script|text)/.test(l);if(c||m.textarea){var f=M.getElementsByTagName("textarea")[0];if(f)x.responseText=f.value,x.status=Number(f.getAttribute("status"))||x.status,x.statusText=f.getAttribute("statusText")||x.statusText;else if(c){var p=M.getElementsByTagName("pre")[0],h=M.getElementsByTagName("body")[0];p?x.responseText=p.textContent?p.textContent:p.innerText:h&&(x.responseText=h.textContent?h.textContent:h.innerText)}}else"xml"==l&&!x.responseXML&&x.responseText&&(x.responseXML=X(x.responseText));try{L=_(x,l,m)}catch(e){i="parsererror",x.error=r=e||i}}catch(e){a("error caught: ",e),i="error",x.error=r=e||i}x.aborted&&(a("upload aborted"),i=null),x.status&&(i=x.status>=200&&x.status<300||304===x.status?"success":"error"),"success"===i?(m.success&&m.success.call(m.context,L,"success",x),S.resolve(x.responseText,"success",x),d&&e.event.trigger("ajaxSuccess",[x,m])):i&&(void 0===r&&(r=x.statusText),m.error&&m.error.call(m.context,x,i,r),S.reject(x,"error",r),d&&e.event.trigger("ajaxError",[x,m,r])),d&&e.event.trigger("ajaxComplete",[x,m]),d&&!--e.active&&e.event.trigger("ajaxStop"),m.complete&&m.complete.call(m.context,x,i),F=!0,m.timeout&&clearTimeout(j),setTimeout(function(){m.iframeTarget||v.remove(),x.responseXML=null},100)}}}var l,f,m,d,h,v,g,x,b,y,T,j,w=c[0],S=e.Deferred();if(r)for(f=0;f<p.length;f++)l=e(p[f]),i?l.prop("disabled",!1):l.removeAttr("disabled");if(m=e.extend(!0,{},e.ajaxSettings,t),m.context=m.context||m,h="jqFormIO"+(new Date).getTime(),m.iframeTarget?(y=(v=e(m.iframeTarget)).attr2("name"))?h=y:v.attr2("name",h):(v=e('<iframe name="'+h+'" src="'+m.iframeSrc+'" />')).css({position:"absolute",top:"-1000px",left:"-1000px"}),g=v[0],x={aborted:0,responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){},abort:function(t){var r="timeout"===t?"timeout":"aborted";a("aborting upload... "+r),this.aborted=1;try{g.contentWindow.document.execCommand&&g.contentWindow.document.execCommand("Stop")}catch(e){}v.attr("src",m.iframeSrc),x.error=r,m.error&&m.error.call(m.context,x,r,t),d&&e.event.trigger("ajaxError",[x,m,r]),m.complete&&m.complete.call(m.context,x,r)}},(d=m.global)&&0==e.active++&&e.event.trigger("ajaxStart"),d&&e.event.trigger("ajaxSend",[x,m]),m.beforeSend&&!1===m.beforeSend.call(m.context,x,m))return m.global&&e.active--,S.reject(),S;if(x.aborted)return S.reject(),S;(b=w.clk)&&(y=b.name)&&!b.disabled&&(m.extraData=m.extraData||{},m.extraData[y]=b.value,"image"==b.type&&(m.extraData[y+".x"]=w.clk_x,m.extraData[y+".y"]=w.clk_y));var k=1,D=2,A=e("meta[name=csrf-token]").attr("content"),E=e("meta[name=csrf-param]").attr("content");E&&A&&(m.extraData=m.extraData||{},m.extraData[E]=A),m.forceSync?o():setTimeout(o,10);var L,M,F,O=50,X=e.parseXML||function(e,t){return window.ActiveXObject?((t=new ActiveXObject("Microsoft.XMLDOM")).async="false",t.loadXML(e)):t=(new DOMParser).parseFromString(e,"text/xml"),t&&t.documentElement&&"parsererror"!=t.documentElement.nodeName?t:null},C=e.parseJSON||function(e){return window.eval("("+e+")")},_=function(t,r,a){var n=t.getResponseHeader("content-type")||"",i="xml"===r||!r&&n.indexOf("xml")>=0,o=i?t.responseXML:t.responseText;return i&&"parsererror"===o.documentElement.nodeName&&e.error&&e.error("parsererror"),a&&a.dataFilter&&(o=a.dataFilter(o,r)),"string"==typeof o&&("json"===r||!r&&n.indexOf("json")>=0?o=C(o):("script"===r||!r&&n.indexOf("javascript")>=0)&&e.globalEval(o)),o};return S}if(!this.length)return a("ajaxSubmit: skipping submit process - no element selected"),this;var s,u,l,c=this;"function"==typeof t&&(t={success:t}),s=this.attr2("method"),(l=(l="string"==typeof(u=this.attr2("action"))?e.trim(u):"")||window.location.href||"")&&(l=(l.match(/^([^#]+)/)||[])[1]),t=e.extend(!0,{url:l,success:e.ajaxSettings.success,type:s||"GET",iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank"},t);var f={};if(this.trigger("form-pre-serialize",[this,t,f]),f.veto)return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"),this;if(t.beforeSerialize&&!1===t.beforeSerialize(this,t))return a("ajaxSubmit: submit aborted via beforeSerialize callback"),this;var m=t.traditional;void 0===m&&(m=e.ajaxSettings.traditional);var d,p=[],h=this.formToArray(t.semantic,p);if(t.data&&(t.extraData=t.data,d=e.param(t.data,m)),t.beforeSubmit&&!1===t.beforeSubmit(h,this,t))return a("ajaxSubmit: submit aborted via beforeSubmit callback"),this;if(this.trigger("form-submit-validate",[h,this,t,f]),f.veto)return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"),this;var v=e.param(h,m);d&&(v=v?v+"&"+d:d),"GET"==t.type.toUpperCase()?(t.url+=(t.url.indexOf("?")>=0?"&":"?")+v,t.data=null):t.data=v;var g=[];if(t.resetForm&&g.push(function(){c.resetForm()}),t.clearForm&&g.push(function(){c.clearForm(t.includeHidden)}),!t.dataType&&t.target){var x=t.success||function(){};g.push(function(r){var a=t.replaceTarget?"replaceWith":"html";e(t.target)[a](r).each(x,arguments)})}else t.success&&g.push(t.success);t.success=function(e,r,a){for(var n=t.context||this,i=0,o=g.length;i<o;i++)g[i].apply(n,[e,r,a||c,c])};var b=e('input[type=file]:enabled[value!=""]',this).length>0,y="multipart/form-data",T=c.attr("enctype")==y||c.attr("encoding")==y,j=n.fileapi&&n.formdata;a("fileAPI :"+j);var w,S=(b||T)&&!j;!1!==t.iframe&&(t.iframe||S)?t.closeKeepAlive?e.get(t.closeKeepAlive,function(){w=o(h)}):w=o(h):w=(b||T)&&j?function(a){for(var n=new FormData,i=0;i<a.length;i++)n.append(a[i].name,a[i].value);if(t.extraData){var o=r(t.extraData);for(i=0;i<o.length;i++)o[i]&&n.append(o[i][0],o[i][1])}t.data=null;var u=e.extend(!0,{},e.ajaxSettings,t,{contentType:!1,processData:!1,cache:!1,type:s||"POST"});t.uploadProgress&&(u.xhr=function(){var e=jQuery.ajaxSettings.xhr();return e.upload&&e.upload.addEventListener("progress",function(e){var r=0,a=e.loaded||e.position,n=e.total;e.lengthComputable&&(r=Math.ceil(a/n*100)),t.uploadProgress(e,a,n,r)},!1),e}),u.data=null;var l=u.beforeSend;return u.beforeSend=function(e,t){t.data=n,l&&l.call(this,e,t)},e.ajax(u)}(h):e.ajax(t),c.removeData("jqxhr").data("jqxhr",w);for(var k=0;k<p.length;k++)p[k]=null;return this.trigger("form-submit-notify",[this,t]),this},e.fn.ajaxForm=function(n){if(n=n||{},n.delegation=n.delegation&&e.isFunction(e.fn.on),!n.delegation&&0===this.length){var i={s:this.selector,c:this.context};return!e.isReady&&i.s?(a("DOM not ready, queuing ajaxForm"),e(function(){e(i.s,i.c).ajaxForm(n)}),this):(a("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)")),this)}return n.delegation?(e(document).off("submit.form-plugin",this.selector,t).off("click.form-plugin",this.selector,r).on("submit.form-plugin",this.selector,n,t).on("click.form-plugin",this.selector,n,r),this):this.ajaxFormUnbind().bind("submit.form-plugin",n,t).bind("click.form-plugin",n,r)},e.fn.ajaxFormUnbind=function(){return this.unbind("submit.form-plugin click.form-plugin")},e.fn.formToArray=function(t,r){var a=[];if(0===this.length)return a;var i=this[0],o=t?i.getElementsByTagName("*"):i.elements;if(!o)return a;var s,u,l,c,f,m,d;for(s=0,m=o.length;s<m;s++)if(f=o[s],(l=f.name)&&!f.disabled)if(t&&i.clk&&"image"==f.type)i.clk==f&&(a.push({name:l,value:e(f).val(),type:f.type}),a.push({name:l+".x",value:i.clk_x},{name:l+".y",value:i.clk_y}));else if((c=e.fieldValue(f,!0))&&c.constructor==Array)for(r&&r.push(f),u=0,d=c.length;u<d;u++)a.push({name:l,value:c[u]});else if(n.fileapi&&"file"==f.type){r&&r.push(f);var p=f.files;if(p.length)for(u=0;u<p.length;u++)a.push({name:l,value:p[u],type:f.type});else a.push({name:l,value:"",type:f.type})}else null!==c&&void 0!==c&&(r&&r.push(f),a.push({name:l,value:c,type:f.type,required:f.required}));if(!t&&i.clk){var h=e(i.clk),v=h[0];(l=v.name)&&!v.disabled&&"image"==v.type&&(a.push({name:l,value:h.val()}),a.push({name:l+".x",value:i.clk_x},{name:l+".y",value:i.clk_y}))}return a},e.fn.formSerialize=function(t){return e.param(this.formToArray(t))},e.fn.fieldSerialize=function(t){var r=[];return this.each(function(){var a=this.name;if(a){var n=e.fieldValue(this,t);if(n&&n.constructor==Array)for(var i=0,o=n.length;i<o;i++)r.push({name:a,value:n[i]});else null!==n&&void 0!==n&&r.push({name:this.name,value:n})}}),e.param(r)},e.fn.fieldValue=function(t){for(var r=[],a=0,n=this.length;a<n;a++){var i=this[a],o=e.fieldValue(i,t);null===o||void 0===o||o.constructor==Array&&!o.length||(o.constructor==Array?e.merge(r,o):r.push(o))}return r},e.fieldValue=function(t,r){var a=t.name,n=t.type,i=t.tagName.toLowerCase();if(void 0===r&&(r=!0),r&&(!a||t.disabled||"reset"==n||"button"==n||("checkbox"==n||"radio"==n)&&!t.checked||("submit"==n||"image"==n)&&t.form&&t.form.clk!=t||"select"==i&&-1==t.selectedIndex))return null;if("select"==i){var o=t.selectedIndex;if(o<0)return null;for(var s=[],u=t.options,l="select-one"==n,c=l?o+1:u.length,f=l?o:0;f<c;f++){var m=u[f];if(m.selected){var d=m.value;if(d||(d=m.attributes&&m.attributes.value&&!m.attributes.value.specified?m.text:m.value),l)return d;s.push(d)}}return s}return e(t).val()},e.fn.clearForm=function(t){return this.each(function(){e("input,select,textarea",this).clearFields(t)})},e.fn.clearFields=e.fn.clearInputs=function(t){var r=/^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;return this.each(function(){var a=this.type,n=this.tagName.toLowerCase();r.test(a)||"textarea"==n?this.value="":"checkbox"==a||"radio"==a?this.checked=!1:"select"==n?this.selectedIndex=-1:"file"==a?/MSIE/.test(navigator.userAgent)?e(this).replaceWith(e(this).clone(!0)):e(this).val(""):t&&(!0===t&&/hidden/.test(a)||"string"==typeof t&&e(this).is(t))&&(this.value="")})},e.fn.resetForm=function(){return this.each(function(){("function"==typeof this.reset||"object"==typeof this.reset&&!this.reset.nodeType)&&this.reset()})},e.fn.enable=function(e){return void 0===e&&(e=!0),this.each(function(){this.disabled=!e})},e.fn.selected=function(t){return void 0===t&&(t=!0),this.each(function(){var r=this.type;if("checkbox"==r||"radio"==r)this.checked=t;else if("option"==this.tagName.toLowerCase()){var a=e(this).parent("select");t&&a[0]&&"select-one"==a[0].type&&a.find("option").selected(!1),this.selected=t}})},e.fn.ajaxSubmit.debug=!1}(jQuery);

 /*
 * TipTip
 * Copyright 2010 Drew Wilson
 * www.drewwilson.com
 * code.drewwilson.com/entry/tiptip-jquery-plugin
 *
 * Version 1.3   -   Updated: Mar. 23, 2010
 *
 * This Plug-In will create a custom tooltip to replace the default
 * browser tooltip. It is extremely lightweight and very smart in
 * that it detects the edges of the browser window and will make sure
 * the tooltip stays within the current window size. As a result the
 * tooltip will adjust itself to be displayed above, below, to the left 
 * or to the right depending on what is necessary to stay within the
 * browser window. It is completely customizable as well via CSS.
 *
 * This TipTip jQuery plug-in is dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
(function($){$.fn.tipTip=function(options){var defaults={activation:"hover",keepAlive:false,maxWidth:"200px",edgeOffset:3,defaultPosition:"top",delay:80,fadeIn:200,fadeOut:50,attribute:"title",content:false,enter:function(){},exit:function(){}};var opts=$.extend(defaults,options);if($("#tiptip_holder").length<=0){var tiptip_holder=$('<div id="tiptip_holder" style="max-width:'+opts.maxWidth+';"></div>');var tiptip_content=$('<div id="tiptip_content"></div>');var tiptip_arrow=$('<div id="tiptip_arrow"></div>');$("html").append(tiptip_holder.html(tiptip_content).prepend(tiptip_arrow.html('<div id="tiptip_arrow_inner"></div>')))}else{var tiptip_holder=$("#tiptip_holder");var tiptip_content=$("#tiptip_content");var tiptip_arrow=$("#tiptip_arrow")}return this.each(function(){var org_elem=$(this);if(opts.content){var org_title=opts.content}else{var org_title=org_elem.attr(opts.attribute)}if(org_title!=""){if(!opts.content){org_elem.removeAttr(opts.attribute)}var timeout=false;if(opts.activation=="hover"){org_elem.hover(function(){active_tiptip()},function(){if(!opts.keepAlive){deactive_tiptip()}});if(opts.keepAlive){tiptip_holder.hover(function(){},function(){deactive_tiptip()})}}else if(opts.activation=="focus"){org_elem.focus(function(){active_tiptip()}).blur(function(){deactive_tiptip()})}else if(opts.activation=="click"){org_elem.click(function(){active_tiptip();return false}).hover(function(){},function(){if(!opts.keepAlive){deactive_tiptip()}});if(opts.keepAlive){tiptip_holder.hover(function(){},function(){deactive_tiptip()})}}function active_tiptip(){opts.enter.call(this);tiptip_content.html(org_title);tiptip_holder.hide().removeAttr("class").css("margin","0");tiptip_arrow.removeAttr("style");var top=parseInt(org_elem.offset()['top']);var left=parseInt(org_elem.offset()['left']);var org_width=parseInt(org_elem.outerWidth());var org_height=parseInt(org_elem.outerHeight());var tip_w=tiptip_holder.outerWidth();var tip_h=tiptip_holder.outerHeight();var w_compare=Math.round((org_width-tip_w)/2);var h_compare=Math.round((org_height-tip_h)/2);var marg_left=Math.round(left+w_compare);var marg_top=Math.round(top+org_height+opts.edgeOffset);var t_class="";var arrow_top="";var arrow_left=Math.round(tip_w-12)/2;if(opts.defaultPosition=="bottom"){t_class="_bottom"}else if(opts.defaultPosition=="top"){t_class="_top"}else if(opts.defaultPosition=="left"){t_class="_left"}else if(opts.defaultPosition=="right"){t_class="_right"}var right_compare=(w_compare+left)<parseInt($(window).scrollLeft());var left_compare=(tip_w+left)>parseInt($(window).width());if((right_compare&&w_compare<0)||(t_class=="_right"&&!left_compare)||(t_class=="_left"&&left<(tip_w+opts.edgeOffset+5))){t_class="_right";arrow_top=Math.round(tip_h-13)/2;arrow_left=-12;marg_left=Math.round(left+org_width+opts.edgeOffset);marg_top=Math.round(top+h_compare)}else if((left_compare&&w_compare<0)||(t_class=="_left"&&!right_compare)){t_class="_left";arrow_top=Math.round(tip_h-13)/2;arrow_left=Math.round(tip_w);marg_left=Math.round(left-(tip_w+opts.edgeOffset+5));marg_top=Math.round(top+h_compare)}var top_compare=(top+org_height+opts.edgeOffset+tip_h+8)>parseInt($(window).height()+$(window).scrollTop());var bottom_compare=((top+org_height)-(opts.edgeOffset+tip_h+8))<0;if(top_compare||(t_class=="_bottom"&&top_compare)||(t_class=="_top"&&!bottom_compare)){if(t_class=="_top"||t_class=="_bottom"){t_class="_top"}else{t_class=t_class+"_top"}arrow_top=tip_h;marg_top=Math.round(top-(tip_h+5+opts.edgeOffset))}else if(bottom_compare|(t_class=="_top"&&bottom_compare)||(t_class=="_bottom"&&!top_compare)){if(t_class=="_top"||t_class=="_bottom"){t_class="_bottom"}else{t_class=t_class+"_bottom"}arrow_top=-12;marg_top=Math.round(top+org_height+opts.edgeOffset)}if(t_class=="_right_top"||t_class=="_left_top"){marg_top=marg_top+5}else if(t_class=="_right_bottom"||t_class=="_left_bottom"){marg_top=marg_top-5}if(t_class=="_left_top"||t_class=="_left_bottom"){marg_left=marg_left+5}tiptip_arrow.css({"margin-left":arrow_left+"px","margin-top":arrow_top+"px"});tiptip_holder.css({"margin-left":marg_left+"px","margin-top":marg_top+"px"}).attr("class","tip"+t_class);if(timeout){clearTimeout(timeout)}timeout=setTimeout(function(){tiptip_holder.stop(true,true).fadeIn(opts.fadeIn)},opts.delay)}function deactive_tiptip(){opts.exit.call(this);if(timeout){clearTimeout(timeout)}tiptip_holder.fadeOut(opts.fadeOut)}}})}})(jQuery);

/*!
* jquery.counterup.js 1.0
*
* Copyright 2013, Benjamin Intal http://gambit.ph @bfintal
* Released under the GPL v2 License
*
* Date: Nov 26, 2013
*/(function(e){"use strict";e.fn.counterUp=function(t){var n=e.extend({time:400,delay:10},t);return this.each(function(){var t=e(this),r=n,i=function(){var e=[],n=r.time/r.delay,i=t.text(),s=/[0-9]+,[0-9]+/.test(i);i=i.replace(/,/g,"");var o=/^[0-9]+$/.test(i),u=/^[0-9]+\.[0-9]+$/.test(i),a=u?(i.split(".")[1]||[]).length:0;for(var f=n;f>=1;f--){var l=parseInt(i/n*f);u&&(l=parseFloat(i/n*f).toFixed(a));if(s)while(/(\d+)(\d{3})/.test(l.toString()))l=l.toString().replace(/(\d+)(\d{3})/,"$1,$2");e.unshift(l)}t.data("counterup-nums",e);t.text("0");var c=function(){t.text(t.data("counterup-nums").shift());if(t.data("counterup-nums").length)setTimeout(t.data("counterup-func"),r.delay);else{delete t.data("counterup-nums");t.data("counterup-nums",null);t.data("counterup-func",null)}};t.data("counterup-func",c);setTimeout(t.data("counterup-func"),r.delay)};t.waypoint(i,{offset:"100%",triggerOnce:!0})})}})(jQuery);

require('rangeslider.js');

var $registration = $('.fos_user_registration_register');
var $login = $('.fos_user_form_login');
var $ajaxLoader = $('.ajax-loader');

var Registration = {
    init: function() {
        $registration.ajaxForm({ target:'.alert', beforeSubmit: Registration.before, success: Registration.after, dataType: 'json' });            
    },
    before: function showRequest(formData, jqForm, options) { 
        jqForm.find($ajaxLoader).fadeIn();
        jqForm.prev('.alert').remove();
        jqForm.find('.alert').remove();
    },
    after : function (responseText, statusText, xhr, $form) {
        if(responseText.status == 'danger') {
            $.each(responseText.errors, function(key, values) {
                var errors = new Array();
                $.each(values, function(i, value) {
                    var li = value;
                    errors.push(li);    
                });

                if (key == 'plainPassword') {
                    var $insertAfter = 'label[for=fos_user_registration_form_'+key+'_first]';
                } else {
                    var $insertAfter = 'label[for=fos_user_registration_form_'+key+']';
                }

                $('<div class="alert alert-danger fade in alert-dismissible" role="alert">'+errors.join(" ")+'</div>').insertAfter($insertAfter);
            });
        } else {
            window.location.href = responseText.redirect;
        }

        $form.find($ajaxLoader).fadeOut();
        $form.resetForm();
    }
};

var Login = {
    init: function() {
        $login.ajaxForm({ target:'.alert', beforeSubmit: Login.before, success: Login.after, dataType: 'json' });            
    },
    before: function showRequest(formData, jqForm, options) { 
        jqForm.find($ajaxLoader).fadeIn();
        jqForm.prev('.alert').remove();
        $('.form-errors').remove();
    },
    after: function (responseText, statusText, xhr, $form) {  
        if(responseText.status == 'success') {
            window.location.href = responseText.redirect
        } else {
            $('<div class="alert fade in alert-dismissible alert-' + responseText.status + '">' + responseText.message + '</div>' ).insertBefore($form);

            $form.find($ajaxLoader).fadeOut();
            $form.resetForm();
        }
    }
};


$(document).ready(function(){
            Registration.init();
            Login.init();
            
            var $container = $('#elements');

            $(".pagination ul li a").on('click', function(e){
                var href = $(this).attr('href');
                var option = $.deparam.querystring( href );
                $('html, body').animate({scrollTop:0}, 500);
                $.bbq.pushState( { page: option.page, layout: option.layout } );

                e.preventDefault();

                return false;
            });

            $(window).bind( 'hashchange', function( event ){
                $('.mask-loader').css({'display': 'block','opacity': 0.95});
                var hashOptions = $.deparam.fragment();
                var page = hashOptions.page != undefined ? hashOptions.page : 1;
                var layout = hashOptions.layout != undefined ? hashOptions.layout : '';

                if(hashOptions.layout == 'grid') {
                    $('.layout-switcher .grid').addClass('active');
                    $('.layout-switcher .list').removeClass('active');
                } else {
                    $('.layout-switcher .list').addClass('active');
                    $('.layout-switcher .grid').removeClass('active');
                }

                $.ajax({
                    type: "GET",
                    url: $container.data('href'),
                    data: { page: page, layout: layout }
                })
                .done(function( content ) {
                    $('.mask-loader').css({'display': 'none','opacity': .0});

                    $container.html($(content).find('#elements').html());
                    $('.show_results').html($(content).find('.show_results').html());
                    $('.pagination').html($(content).find('.pagination').html());

                    $(".pagination ul li a").on('click', function(e){
                        var href = $(this).attr('href');
                        var option = $.deparam.querystring( href );
                        $('html, body').animate({scrollTop:0}, 500);
                        $.bbq.pushState( { page: option.page, layout: option.layout } );

                        e.preventDefault();

                        return false;
                    });
                });
            }).trigger('hashchange');
        
            /*--------------------------------------------------*/
            /*  Navigation
            /*--------------------------------------------------*/
            var jPanelMenu = $.jPanelMenu({
              menu: '#responsive',
              animated: false,
              duration: 200,
              keyboardShortcuts: false,
              closeOnContentClick: true
            });

            // Menu Trigger
            $('.menu-trigger').on('click', function(){

              var jpm = $(this);

              if( jpm.hasClass('active') )
              {
                jPanelMenu.off();
                jpm.removeClass('active');
              }
              else
              {
                jPanelMenu.on();
                jPanelMenu.open();
                jpm.addClass('active');

                    // Removes SuperFish Styles
                    $('#jPanelMenu-menu').removeClass('menu');
                    $('ul#jPanelMenu-menu li').removeClass('dropdown');
                    $('ul#jPanelMenu-menu li ul').removeAttr('style');
                    $('ul#jPanelMenu-menu li div').removeClass('mega').removeAttr('style');
                    $('ul#jPanelMenu-menu li div div').removeClass('mega-container');
              }
              return false;
            });

            $(window).resize(function (){
                    var winWidth = $(window).width();
                    if(winWidth>992) {
                            jPanelMenu.close();
                    }
            });

        /*  User Menu */
        $('.user-menu').on('click', function(){
                    $(this).toggleClass('active');
            });


            /*----------------------------------------------------*/
            /*  Sticky Header 
            /*----------------------------------------------------*/
            $( "#header" ).not( "#header.not-sticky" ).clone(true).addClass('cloned unsticky').insertAfter( "#header" );
            $( "#navigation.style-2" ).clone(true).addClass('cloned unsticky').insertAfter( "#navigation.style-2" );

            // Logo for header style 2
            $( "#logo .sticky-logo" ).clone(true).prependTo("#navigation.style-2.cloned ul#responsive");


            // sticky header script
            var headerOffset = $("#header-container").height() * 2; // height on which the sticky header will shows

            $(window).scroll(function(){
                    if($(window).scrollTop() >= headerOffset){
                            $("#header.cloned").addClass('sticky').removeClass("unsticky");
                            $("#navigation.style-2.cloned").addClass('sticky').removeClass("unsticky");
                    } else {
                            $("#header.cloned").addClass('unsticky').removeClass("sticky");
                            $("#navigation.style-2.cloned").addClass('unsticky').removeClass("sticky");
                    }
            });


            /*----------------------------------------------------*/
            /*  Back to Top
            /*----------------------------------------------------*/
            var pxShow = 600; // height on which the button will show
            var scrollSpeed = 500; // how slow / fast you want the button to scroll to top.

            $(window).scroll(function(){
             if($(window).scrollTop() >= pxShow){
                    $("#backtotop").addClass('visible');
             } else {
                    $("#backtotop").removeClass('visible');
             }
            });

            $('#backtotop a').on('click', function(){
             $('html, body').animate({scrollTop:0}, scrollSpeed);
             return false;
            });


            /*----------------------------------------------------*/
            /*  Inline CSS replacement for backgrounds etc.
            /*----------------------------------------------------*/
            function inlineCSS() {

                    // Common Inline CSS
                    $(".main-search-container, section.fullwidth, .listing-slider .item, .address-container, .img-box-background, .image-edge, .edge-bg").each(function() {
                            var attrImageBG = $(this).attr('data-background-image');
                            var attrColorBG = $(this).attr('data-background-color');

                    if(attrImageBG !== undefined) {
                        $(this).css('background-image', 'url('+attrImageBG+')');
                    }

                    if(attrColorBG !== undefined) {
                        $(this).css('background', ''+attrColorBG+'');
                    }
                    });

            }

            // Init
            inlineCSS();

            function parallaxBG() {

                    $('.parallax').prepend('<div class="parallax-overlay"></div>');

                    $( ".parallax").each(function() {
                            var attrImage = $(this).attr('data-background');
                            var attrColor = $(this).attr('data-color');
                            var attrOpacity = $(this).attr('data-color-opacity');

                    if(attrImage !== undefined) {
                        $(this).css('background-image', 'url('+attrImage+')');
                    }

                    if(attrColor !== undefined) {
                        $(this).find(".parallax-overlay").css('background-color', ''+attrColor+'');
                    }

                    if(attrOpacity !== undefined) {
                        $(this).find(".parallax-overlay").css('opacity', ''+attrOpacity+'');
                    }

                    });
            }

            parallaxBG();



        /*----------------------------------------------------*/
        /*  Image Box 
        /*----------------------------------------------------*/
            $('.category-box').each(function(){

                    // add a photo container
                    $(this).append('<div class="category-box-background"></div>');

                    // set up a background image for each tile based on data-background-image attribute
                    $(this).children('.category-box-background').css({'background-image': 'url('+ $(this).attr('data-background-image') +')'});

                    // background animation on mousemove
                    // $(this).on('mousemove', function(e){
                    //   $(this).children('.category-box-background').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
                    // })
            });


        /*----------------------------------------------------*/
        /*  Image Box 
        /*----------------------------------------------------*/
            $('.img-box').each(function(){
                    $(this).append('<div class="img-box-background"></div>');
                    $(this).children('.img-box-background').css({'background-image': 'url('+ $(this).attr('data-background-image') +')'});
            });



            /*----------------------------------------------------*/
            /*  Parallax
            /*----------------------------------------------------*/

            /* detect touch */
            if("ontouchstart" in window){
                document.documentElement.className = document.documentElement.className + " touch";
            }
            if(!$("html").hasClass("touch")){
                /* background fix */
                $(".parallax").css("background-attachment", "fixed");
            }

            /* fix vertical when not overflow
            call fullscreenFix() if .fullscreen content changes */
            function fullscreenFix(){
                var h = $('body').height();
                // set .fullscreen height
                $(".content-b").each(function(i){
                    if($(this).innerHeight() > h){ $(this).closest(".fullscreen").addClass("overflow");
                    }
                });
            }
            $(window).resize(fullscreenFix);
            fullscreenFix();

            /* resize background images */
            function backgroundResize(){
                var windowH = $(window).height();
                $(".parallax").each(function(i){
                    var path = $(this);
                    // variables
                    var contW = path.width();
                    var contH = path.height();
                    var imgW = path.attr("data-img-width");
                    var imgH = path.attr("data-img-height");
                    var ratio = imgW / imgH;
                    // overflowing difference
                    var diff = 100;
                    diff = diff ? diff : 0;
                    // remaining height to have fullscreen image only on parallax
                    var remainingH = 0;
                    if(path.hasClass("parallax") && !$("html").hasClass("touch")){
                        //var maxH = contH > windowH ? contH : windowH;
                        remainingH = windowH - contH;
                    }
                    // set img values depending on cont
                    imgH = contH + remainingH + diff;
                    imgW = imgH * ratio;
                    // fix when too large
                    if(contW > imgW){
                        imgW = contW;
                        imgH = imgW / ratio;
                    }
                    //
                    path.data("resized-imgW", imgW);
                    path.data("resized-imgH", imgH);
                    path.css("background-size", imgW + "px " + imgH + "px");
                });
            }


            $(window).resize(backgroundResize);
            $(window).focus(backgroundResize);
            backgroundResize();

            /* set parallax background-position */
            function parallaxPosition(e){
                var heightWindow = $(window).height();
                var topWindow = $(window).scrollTop();
                var bottomWindow = topWindow + heightWindow;
                var currentWindow = (topWindow + bottomWindow) / 2;
                $(".parallax").each(function(i){
                    var path = $(this);
                    var height = path.height();
                    var top = path.offset().top;
                    var bottom = top + height;
                    // only when in range
                    if(bottomWindow > top && topWindow < bottom){
                        //var imgW = path.data("resized-imgW");
                        var imgH = path.data("resized-imgH");
                        // min when image touch top of window
                        var min = 0;
                        // max when image touch bottom of window
                        var max = - imgH + heightWindow;
                        // overflow changes parallax
                        var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
                        top = top - overflowH;
                        bottom = bottom + overflowH;


                        // value with linear interpolation
                        // var value = min + (max - min) * (currentWindow - top) / (bottom - top);
                        var value = 0;
                                    if ( $('.parallax').is(".titlebar") ) {
                                        value = min + (max - min) * (currentWindow - top) / (bottom - top) *2;
                                    } else {
                                            value = min + (max - min) * (currentWindow - top) / (bottom - top);
                                    }

                        // set background-position
                        var orizontalPosition = path.attr("data-oriz-pos");
                        orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
                        $(this).css("background-position", orizontalPosition + " " + value + "px");
                    }
                });
            }
            if(!$("html").hasClass("touch")){
                $(window).resize(parallaxPosition);
                //$(window).focus(parallaxPosition);
                $(window).scroll(parallaxPosition);
                parallaxPosition();
            }

            // Jumping background fix for IE
            if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
                $('body').on("mousewheel", function () {
                    event.preventDefault(); 

                    var wheelDelta = event.wheelDelta;
                    var currentScrollPosition = window.pageYOffset;
                    window.scrollTo(0, currentScrollPosition - wheelDelta);
                });
            }


        /*----------------------------------------------------*/
        /*  Chosen Plugin
        /*----------------------------------------------------*/

        var config = {
          '.chosen-select'           : {disable_search_threshold: 10, width:"100%"},
          '.chosen-select-deselect'  : {allow_single_deselect:true, width:"100%"},
          '.chosen-select-no-single' : {disable_search_threshold:100, width:"100%"},
          '.chosen-select-no-single.no-search' : {disable_search_threshold:10, width:"100%"},
          '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
          '.chosen-select-width'     : {width:"95%"}
        };

        for (var selector in config) {
                    if (config.hasOwnProperty(selector)) {
                  $(selector).chosen(config[selector]);
                    }
        }

            /*----------------------------------------------------*/
        /*  Magnific Popup
        /*----------------------------------------------------*/

            $('.mfp-gallery-container').each(function() { // the containers for all your galleries

                    $(this).magnificPopup({
                             type: 'image',
                             delegate: 'a.mfp-gallery',

                             fixedContentPos: true,
                             fixedBgPos: true,

                             overflowY: 'auto',

                             closeBtnInside: false,
                             preloader: true,

                             removalDelay: 0,
                             mainClass: 'mfp-fade',

                             gallery:{enabled:true, tCounter: ''}
                    });
            });

            $('.popup-with-zoom-anim').magnificPopup({
                     type: 'inline',

                     fixedContentPos: false,
                     fixedBgPos: true,

                     overflowY: 'auto',

                     closeBtnInside: true,
                     preloader: false,

                     midClick: true,
                     removalDelay: 300,
                     mainClass: 'my-mfp-zoom-in'
            });

            $('.mfp-image').magnificPopup({
                     type: 'image',
                     closeOnContentClick: true,
                     mainClass: 'mfp-fade',
                     image: {
                              verticalFit: true
                     }
            });

            $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
                     disableOn: 700,
                     type: 'iframe',
                     mainClass: 'mfp-fade',
                     removalDelay: 160,
                     preloader: false,

                     fixedContentPos: false
            });



            /*----------------------------------------------------*/
        /*  Slick Carousel
        /*----------------------------------------------------*/

            $('.fullwidth-slick-carousel').slick({
              centerMode: true,
              centerPadding: '15%',
              slidesToShow: 3,
              dots: true,
              arrows: false,
              responsive: [
                    {
                      breakpoint: 1441,
                      settings: {
                        centerPadding: '10%',
                        slidesToShow: 3
                      }
                    },
                    {
                      breakpoint: 1025,
                      settings: {
                        centerPadding: '10px',
                        slidesToShow: 2,
                      }
                    },
                    {
                      breakpoint: 767,
                      settings: {
                        centerPadding: '10px',
                        slidesToShow: 1
                      }
                    }
              ]
            });


            $('.testimonial-carousel').slick({
              centerMode: true,
              centerPadding: '34%',
              slidesToShow: 1,
              dots: true,
              arrows: false,
              responsive: [
                    {
                      breakpoint: 1025,
                      settings: {
                        centerPadding: '10px',
                        slidesToShow: 2,
                      }
                    },
                    {
                      breakpoint: 767,
                      settings: {
                        centerPadding: '10px',
                        slidesToShow: 1
                      }
                    }
              ]
            });


             $('.listing-slider').slick({
                    centerMode: true,
                    centerPadding: '20%',
                    slidesToShow: 2, 
                    responsive: [
                            {
                              breakpoint: 1367,
                              settings: {
                                centerPadding: '15%'
                              }
                            },
                            {
                              breakpoint: 1025,
                              settings: {
                                centerPadding: '0'
                              }
                            },
                            {
                              breakpoint: 767,
                              settings: {
                                centerPadding: '0',
                                slidesToShow: 1
                              }
                            }
                    ]
            });


            $('.simple-slick-carousel').slick({
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    dots: true,
                    arrows: true,
                    responsive: [
                        {
                          breakpoint: 992,
                          settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                          }
                        },
                        {
                          breakpoint: 769,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                          }
                        }
              ]
            });


            $('.simple-fw-slick-carousel').slick({
                    infinite: true,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false,

                    responsive: [
                    {
                      breakpoint: 1610,
                      settings: {
                            slidesToShow: 4,
                      }
                    },
                    {
                      breakpoint: 1365,
                      settings: {
                            slidesToShow: 3,
                      }
                    },
                    {
                      breakpoint: 1024,
                      settings: {
                            slidesToShow: 2,
                      }
                    },
                    {
                      breakpoint: 767,
                      settings: {
                            slidesToShow: 1,
                      }
                    }
                    ]
            });


            $('.logo-slick-carousel').slick({
                    infinite: true,
                    slidesToShow: 5,
                    slidesToScroll: 4,
                    dots: true,
                    arrows: true,
                    responsive: [
                        {
                          breakpoint: 992,
                          settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                          }
                        },
                        {
                          breakpoint: 769,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                          }
                        }
              ]
            });


            /*----------------------------------------------------*/
            /*  Tabs
            /*----------------------------------------------------*/ 

            var $tabsNav    = $('.tabs-nav'),
            $tabsNavLis = $tabsNav.children('li');

            $tabsNav.each(function() {
                     var $this = $(this);

                     $this.next().children('.tab-content').stop(true,true).hide()
                     .first().show();

                     $this.children('li').first().addClass('active').stop(true,true).show();
            });

            $tabsNavLis.on('click', function(e) {
                     var $this = $(this);

                     $this.siblings().removeClass('active').end()
                     .addClass('active');

                     $this.parent().next().children('.tab-content').stop(true,true).hide()
                     .siblings( $this.find('a').attr('href') ).fadeIn();

                     e.preventDefault();
            });
            var hash = window.location.hash;
            var anchor = $('.tabs-nav a[href="' + hash + '"]');
            if (anchor.length === 0) {
                     $(".tabs-nav li:first").addClass("active").show(); //Activate first tab
                     $(".tab-content:first").show(); //Show first tab content
            } else {
                     anchor.parent('li').click();
            }


            /*----------------------------------------------------*/
            /*  Accordions
            /*----------------------------------------------------*/
            var $accor = $('.accordion');

             $accor.each(function() {
                     $(this).toggleClass('ui-accordion ui-widget ui-helper-reset');
                     $(this).find('h3').addClass('ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all');
                     $(this).find('div').addClass('ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom');
                     $(this).find("div").hide();

            });

            var $trigger = $accor.find('h3');

            $trigger.on('click', function(e) {
                     var location = $(this).parent();

                     if( $(this).next().is(':hidden') ) {
                              var $triggerloc = $('h3',location);
                              $triggerloc.removeClass('ui-accordion-header-active ui-state-active ui-corner-top').next().slideUp(300);
                              $triggerloc.find('span').removeClass('ui-accordion-icon-active');
                              $(this).find('span').addClass('ui-accordion-icon-active');
                              $(this).addClass('ui-accordion-header-active ui-state-active ui-corner-top').next().slideDown(300);
                     }
                      e.preventDefault();
            });


            /*----------------------------------------------------*/
            /*	Toggle
            /*----------------------------------------------------*/

            $(".toggle-container").hide();

            $('.trigger, .trigger.opened').on('click', function(a){
                    $(this).toggleClass('active');
                    a.preventDefault();
            });

            $(".trigger").on('click', function(){
                    $(this).next(".toggle-container").slideToggle(300);
            });

            $(".trigger.opened").addClass("active").next(".toggle-container").show();


            /*----------------------------------------------------*/
            /*  Tooltips
            /*----------------------------------------------------*/

            $(".tooltip.top").tipTip({
              defaultPosition: "top"
            });

            $(".tooltip.bottom").tipTip({
              defaultPosition: "bottom"
            });

            $(".tooltip.left").tipTip({
              defaultPosition: "left"
            });

            $(".tooltip.right").tipTip({
              defaultPosition: "right"
            });



        /*----------------------------------------------------*/
        /*  Like Icon Trigger
        /*----------------------------------------------------*/
        $('.like-icon, .widget-button, .like-button').on('click', function(e){
            e.preventDefault();
                    $(this).toggleClass('liked');
                    $(this).children('.like-icon').toggleClass('liked');
            });

        /*----------------------------------------------------*/
        /*  Searh Form More Options
        /*----------------------------------------------------*/
        $('.more-search-options-trigger').on('click', function(e){
            e.preventDefault();
                    $('.more-search-options, .more-search-options-trigger').toggleClass('active');
                    $('.more-search-options.relative').animate({height: 'toggle', opacity: 'toggle'}, 300);
            });


        /*----------------------------------------------------*/
        /*  Half Screen Map Adjustments
        /*----------------------------------------------------*/
            $(window).on('load resize', function() {
                    var winWidth = $(window).width();
                    var headerHeight = $("#header-container").height(); // height on which the sticky header will shows

                    $('.fs-inner-container, .fs-inner-container.map-fixed, #dashboard').css('padding-top', headerHeight);

                    if(winWidth<992) {
                            $('.fs-inner-container.map-fixed').insertBefore('.fs-inner-container.content');
                    } else {
                            $('.fs-inner-container.content').insertBefore('.fs-inner-container.map-fixed');
                    }

            });


        /*----------------------------------------------------*/
        /*  Counters
        /*----------------------------------------------------*/
        $(window).on('load resize', function() {
                    $('.dashboard-stat-content h4').counterUp({
                    delay: 100,
                    time: 800
                });
        });


        /*----------------------------------------------------*/
        /*  Rating Script Init
        /*----------------------------------------------------*/

            // Leave Rating 
            $('.leave-rating input').change(function () {
                    var $radio = $(this);
                    $('.leave-rating .selected').removeClass('selected');
                    $radio.closest('label').addClass('selected');
            });


            /*----------------------------------------------------*/
            /* Dashboard Scripts
            /*----------------------------------------------------*/
        $('.dashboard-nav ul li a').on('click', function(){
                    if ($(this).closest('li').has('ul').length) {
                            $(this).parent('li').toggleClass('active');
                    }
            });

        // Dashbaord Nav Scrolling
            $(window).on('load resize', function() {
                    var wrapperHeight = window.innerHeight;
                    var headerHeight = $("#header-container").height();
                    var winWidth = $(window).width();

                    if(winWidth>992) {
                            $(".dashboard-nav-inner").css('max-height', wrapperHeight-headerHeight);
                    } else {
                            $(".dashboard-nav-inner").css('max-height', '');
                    }
            });


        // Tooltip
            $(".tip").each(function() {
                    var tipContent = $(this).attr('data-tip-content');
                    $(this).append('<div class="tip-content">'+ tipContent + '</div>');
            });


        // Switcher
            $(".add-listing-section").each(function() {

                    var switcherSection = $(this);
                    var switcherInput = $(this).find('.switch input');

                    if(switcherInput.is(':checked')){
                            $(switcherSection).addClass('switcher-on');
                    } 

                    switcherInput.change(function(){
                            if(this.checked===true){
                                    $(switcherSection).addClass('switcher-on');
                            } else {
                                    $(switcherSection).removeClass('switcher-on');
                            }
                    });

            });


            // Responsive Nav Trigger
        $('.dashboard-responsive-nav-trigger').on('click', function(e){
            e.preventDefault();
                    $(this).toggleClass('active');

                    var dashboardNavContainer = $('body').find(".dashboard-nav");

                    if( $(this).hasClass('active') ){
                            $(dashboardNavContainer).addClass('active');
                    } else {
                            $(dashboardNavContainer).removeClass('active');
                    }

            });

        // Dashbaord Messages Alignment
            $(window).on('load resize', function() {
                    var msgContentHeight = $(".message-content").outerHeight();
                    var msgInboxHeight = $(".messages-inbox ul").height();

                    if( msgContentHeight > msgInboxHeight ){
                            $(".messages-container-inner .messages-inbox ul").css('max-height', msgContentHeight)
                    }
            });



        /*----------------------------------------------------*/
            /* Pricing List
            /*----------------------------------------------------*/
            function newMenuItem() {
                    var newElem = $('tr.pricing-list-item.pattern').first().clone();
                    newElem.find('input').val('');
                    newElem.appendTo('table#pricing-list-container');
            }

            if ($("table#pricing-list-container").is('*')) {
                    $('.add-pricing-list-item').on('click', function(e) {
                            e.preventDefault();
                            newMenuItem();
                    });

                    // remove ingredient
                    $(document).on( "click", "#pricing-list-container .delete", function(e) {
                            e.preventDefault();
                            $(this).parent().parent().remove();
                    });

                    // add submenu
                    $('.add-pricing-submenu').on('click', function(e) {
                            e.preventDefault();

                            var newElem = $(''+
                                    '<tr class="pricing-list-item pricing-submenu">'+
                                            '<td>'+
                                                    '<div class="fm-move"><i class="sl sl-icon-cursor-move"></i></div>'+
                                                    '<div class="fm-input"><input type="text" placeholder="Category Title" /></div>'+
                                                    '<div class="fm-close"><a class="delete" href="#"><i class="fa fa-remove"></i></a></div>'+
                                            '</td>'+
                                    '</tr>');

                            newElem.appendTo('table#pricing-list-container');
                    });

                    $('table#pricing-list-container tbody').sortable({
                            forcePlaceholderSize: true,
                            forceHelperSize: false,
                            placeholder : 'sortableHelper',
                            zIndex: 999990,
                            opacity: 0.6,
                            tolerance: "pointer",
                            start: function(e, ui ){
                                 ui.placeholder.height(ui.helper.outerHeight());
                            }
                    });
            }


        // Unit character
        var fieldUnit = $('.pricing-price').children('input').attr('data-unit');
        $('.pricing-price').children('input').before('<i class="data-unit">'+ fieldUnit + '</i>');



            /*----------------------------------------------------*/
            /*  Notifications
            /*----------------------------------------------------*/
            $("a.close").removeAttr("href").on('click', function(){

                    function slideFade(elem) {
                            var fadeOut = { opacity: 0, transition: 'opacity 0.5s' };
                            elem.css(fadeOut).slideUp();
                    }
                    slideFade($(this).parent());

            });


            /*----------------------------------------------------*/
            /* Panel Dropdown
            /*----------------------------------------------------*/
        function close_panel_dropdown() {
                    $('.panel-dropdown').removeClass("active");
                    $('.fs-inner-container.content').removeClass("faded-out");
        }

        $('.panel-dropdown a').on('click', function(e) {

                    if ( $(this).parent().is(".active") ) {
                close_panel_dropdown();
            } else {
                close_panel_dropdown();
                $(this).parent().addClass('active');
                            $('.fs-inner-container.content').addClass("faded-out");
            }

            e.preventDefault();
        }); 

        // Apply / Close buttons
        $('.panel-buttons button').on('click', function(e) {
                $('.panel-dropdown').removeClass('active');
                    $('.fs-inner-container.content').removeClass("faded-out");
        });

        // Closes dropdown on click outside the conatainer
            var mouse_is_inside = false;

            $('.panel-dropdown').hover(function(){ 
                mouse_is_inside=true; 
            }, function(){ 
                mouse_is_inside=false; 
            });

            $("body").mouseup(function(){ 
                if(! mouse_is_inside) close_panel_dropdown();
            });

        // "All" checkbox
        $('.checkboxes.categories input').on('change', function() {
            if($(this).hasClass('all')){
                $(this).parents('.checkboxes').find('input').prop('checked', false);
                $(this).prop('checked', true);
            } else {
                $('.checkboxes input.all').prop('checked', false);
            }
        });


            $('input[type="range"].distance-radius').rangeslider({
                polyfill : false,
                onInit : function() {
                    this.output = $( '<div class="range-output" />' ).insertBefore( this.$range ).html( this.$element.val() );

                        var radiustext = $('.distance-radius').attr('data-title');
                        $('.range-output').after('<i class="data-radius-title">'+ radiustext + '</i>');

                },
                onSlide : function( position, value ) {
                    this.output.html( value );
                }
            });


        /*----------------------------------------------------*/
        /*  Show More Button
        /*----------------------------------------------------*/
        $('.show-more-button').on('click', function(e){
            e.preventDefault();
            $(this).toggleClass('active');

                    $('.show-more').toggleClass('visible');
                    if ( $('.show-more').is(".visible") ) {

                            var el = $('.show-more'),
                                    curHeight = el.height(),
                                    autoHeight = el.css('height', 'auto').height();
                                    el.height(curHeight).animate({height: autoHeight}, 400);


                    } else { $('.show-more').animate({height: '450px'}, 400); }

            });


            /*----------------------------------------------------*/
            /* Listing Page Nav
            /*----------------------------------------------------*/

        $(window).on('load resize', function() {
          var containerWidth = $(".container").width();
          $('.listing-nav-container.cloned .listing-nav').css('width', containerWidth);
        });

            if(document.getElementById("listing-nav") !== null) {
                    $(window).scroll(function(){
                            var window_top = $(window).scrollTop();
                            var div_top = $('.listing-nav').not('.listing-nav-container.cloned .listing-nav').offset().top + 90;
                        if (window_top > div_top) {
                            $('.listing-nav-container.cloned').addClass('stick');
                        } else {
                            $('.listing-nav-container.cloned').removeClass('stick');
                        }
                    });
            }

            $( ".listing-nav-container" ).clone(true).addClass('cloned').prependTo("body");


        // Smooth scrolling using scrollto.js
            $('.listing-nav a, a.listing-address, .star-rating a').on('click', function(e) {
            e.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash, { gap: {y: -20} }); 
        });

            $(".listing-nav li:first-child a, a.add-review-btn, a[href='#add-review']").on('click', function(e) {
            e.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash, { gap: {y: -100} }); 
        });


        // Highlighting functionality.
            $(window).on('load resize', function() {
                    var aChildren = $(".listing-nav li").children();
                    var aArray = [];
                    for (var i=0; i < aChildren.length; i++) {    
                        var aChild = aChildren[i];
                        var ahref = $(aChild).attr('href');
                        aArray.push(ahref);
                    }

                    $(window).scroll(function(){
                        var windowPos = $(window).scrollTop(); 
                        for (var i=0; i < aArray.length; i++) {
                            var theID = aArray[i];
                            var divPos = $(theID).offset().top - 150;
                            var divHeight = $(theID).height();
                            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                                $("a[href='" + theID + "']").addClass("active");
                            } else {
                                $("a[href='" + theID + "']").removeClass("active");
                            }
                        }
                    });
            });


            /*----------------------------------------------------*/
            /*  Progress Button
            /*----------------------------------------------------*/
        $('.progress-button').each(function(){
            $(this).append('<div class="progress-bar"></div>');

                    $(this).on('click', function() {
                            var progressButton = $(this);

                            progressButton.children('.progress-bar').addClass('active');

                            // progress bar
                            setTimeout(function(){
                            progressButton.children('.progress-bar').removeClass('active');
                            }, 1500);

                            setTimeout(function(){
                            progressButton.children('.progress-bar').removeClass('active');
                            }, 1500);


                            // icon
                            setTimeout(function(){
                            progressButton.addClass('done');
                            }, 1800);

                            setTimeout(function(){
                            progressButton.removeClass('done');
                            }, 3500);

                    });
        });


            /*----------------------------------------------------*/
            /*  Contact Form
            /*----------------------------------------------------*/

        var shake = "No";

        $('#message').hide();

        // Add validation parts
        $('#contact input[type=text], #contact input[type=number], #contact input[type=email], #contact input[type=url], #contact input[type=tel], #contact select, #contact textarea').each(function(){

        });

             // Validate as you type
               $('#name, #comments, #subject').focusout(function() {
                   if (!$(this).val()) {
                       $(this).addClass('error').parent().find('mark').removeClass('valid').addClass('error');
                   }
                   else {
                       $(this).removeClass('error').parent().find('mark').removeClass('error').addClass('valid');
                   }
                     $('#submit')
                       .prop('disabled',false)
                       .removeClass('disabled');
               });
               $('#email').focusout(function() {
                   if (!$(this).val() || !isEmail($(this).val())) {
                       $(this).addClass('error').parent().find('mark').removeClass('valid').addClass('error');
                   } else {
                       $(this).removeClass('error').parent().find('mark').removeClass('error').addClass('valid');
                   }
               });

               $('#email').focusin(function() {
                    $('#submit')
                       .prop('disabled',false)
                       .removeClass('disabled');
               });

            $('#submit').on('click', function() {
            $("#contact-message").slideUp(200,function() {
                $('#contact-message').hide();

                // Kick in Validation
                $('#name, #subject, #phone, #comments, #website, #email').triggerHandler("focusout");

                if ($('#contact mark.error').size()>0) {
                    if(shake == "Yes") {
                        $('#contact').effect('shake', { times:2 }, 75, function(){
                            $('#contact input.error:first, #contact textarea.error:first').focus();
                        });
                    } else $('#contact input.error:first, #contact textarea.error:first').focus();

                    return false;
                }

            });
        });

        $('#contactform').submit(function(){

            if ($('#contact mark.error').size()>0) {
                if(shake == "Yes") {
                $('#contact').effect('shake', { times:2 }, 75);
                }
                return false;
            }

            var action = $(this).attr('action');

            $('#contact #submit').after('<img src="images/loader.gif" class="loader" />');

            $('#submit')
                .prop('disabled',true)
                .addClass('disabled');

            $.post(action, $('#contactform').serialize(),
                function(data){
                    $('#contact-message').html( data );
                    $('#contact-message').slideDown();
                    $('#contactform img.loader').fadeOut('slow',function(){$(this).remove();});
                    // $('#contactform #submit').removeAttr('disabled');
                    if(data.match('success') !== null) $('#contactform').slideUp('slow');

                }
            );

            return false;

        });

        function isEmail(emailAddress) {

            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

            return pattern.test(emailAddress);
        }

        //Place Autocomplete Search Google
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('place'), {
            //types: ['(cities)'],
            componentRestrictions: {country: 'it'}
        });

       /*google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();

            if (place.address_components) {
              console.log(place.address_components);
            }
        });*/

        $('.geolocation').on('click', function(){
            $(this).removeAttr('href');
            getLocation();
        });

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                window.alert("Geolocation is not supported by this browser.");
            }
        }

        function showPosition(position) {
            geocodeLatLng(new google.maps.Geocoder, position.coords);
        }

        function geocodeLatLng(geocoder, coords) {
            var latlng = {lat: parseFloat(coords.latitude), lng: parseFloat(coords.longitude)};
            geocoder.geocode({'location': latlng}, function(results, status) {
              if (status === 'OK') {
                if (results[0]) {
                  $('#place').val(results[0].formatted_address);
                } else {
                  window.alert('No results found');
                }
              } else {
                window.alert('Geocoder failed due to: ' + status);
              }
            });
        }

        $('.layout-switcher a').on('click', function(){
            var layout = $(this).attr('href').replace( /^#/, '' );
            var option = $.deparam( layout, true );

            $.bbq.pushState( option );

            return false;
        });

        $('img').lazyload({
            effect : "fadeIn",
            placeholder : "../images/loading.gif" ,
            failure_limit: Math.max($('img').length - 1, 0)
        });

    // ------------------ End Document ------------------ //
    });

/*!
 * jquery.scrollto.js 0.0.1 - https://github.com/yckart/jquery.scrollto.js
 * Copyright (c) 2012 Yannick Albert (http://yckart.com)
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php).
 **/

$.scrollTo = $.fn.scrollTo = function(x, y, options){
    if (!(this instanceof $)) return $.fn.scrollTo.apply($('html, body'), arguments);

    options = $.extend({}, {
        gap: {
            x: 0,
            y: 0
        },
        animation: {
            easing: 'swing',
            duration: 600,
            complete: $.noop,
            step: $.noop
        }
    }, options);

    return this.each(function(){
        var elem = $(this);
        elem.stop().animate({
            scrollLeft: !isNaN(Number(x)) ? x : $(y).offset().left + options.gap.x,
            scrollTop: !isNaN(Number(y)) ? y : $(y).offset().top + options.gap.y
        }, options.animation);
    });
};