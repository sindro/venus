-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Nov 09, 2017 alle 11:39
-- Versione del server: 5.7.14
-- Versione PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `venus`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lft` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `category`
--

INSERT INTO `category` (`id`, `parent_id`, `title`, `lft`, `lvl`, `rgt`, `root`, `is_public`, `created`, `updated`, `deletedAt`, `slug`, `image_id`) VALUES
(1, NULL, 'Relax e Percorsi Benessere Spa', 2, 0, 9, 1, 1, '2017-07-29 10:03:10', '2017-09-10 10:18:19', NULL, 'relax-e-percorsi-benessere-spa', 10),
(2, 1, 'Bagno Rasul', 5, 1, 6, 2, 1, '2017-07-29 10:03:10', '2017-08-27 15:49:16', NULL, 'bagno-rasul', NULL),
(3, 1, 'Rituale Hammam', 3, 1, 4, 3, 1, '2017-07-29 10:03:10', '2017-08-27 15:49:16', NULL, 'rituale-hammam', NULL),
(4, 1, 'Percorso Acqua e Vapore', 7, 1, 8, 4, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'percorso-acqua-e-vapore', NULL),
(5, NULL, 'Percorso di Coppia', 94, 0, 95, 5, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'percorso-di-coppia', NULL),
(6, NULL, 'Trattamenti Termali', 10, 0, 19, 6, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'trattamenti-termali', NULL),
(7, 6, 'Total body relax', 11, 1, 12, 7, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'total-body-relax', NULL),
(8, 6, 'Bagno di vapore', 13, 1, 14, 8, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'bagno-di-vapore', NULL),
(9, 6, 'Pinda Sveda Nam', 15, 1, 16, 9, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'pinda-sveda-nam', NULL),
(10, 6, 'Stone Massage', 17, 1, 18, 10, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'stone-massage', NULL),
(11, NULL, 'Trattamenti Dimagranti', 20, 0, 33, 11, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'trattamenti-dimagranti', NULL),
(12, 11, 'Anticellulite', 21, 1, 22, 12, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'anticellulite', NULL),
(13, 11, 'Dimagrimento Localizzato', 23, 1, 24, 13, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'dimagrimento-localizzato', NULL),
(14, 11, 'Viso e Decolletè', 25, 1, 26, 14, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'viso-e-decollete', NULL),
(15, 11, 'Cavitazione', 27, 1, 28, 15, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'cavitazione', NULL),
(16, 34, 'Radiofrequenza', 69, 1, 70, 16, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'radiofrequenza', NULL),
(17, 11, 'Massaggio Endodermico', 31, 1, 32, 17, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'massaggio-endodermico', NULL),
(18, NULL, 'Trattamenti e Massaggio Corpo', 34, 0, 43, 18, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'trattamenti-e-massaggio-corpo', NULL),
(19, 18, 'Fangoterapia', 35, 1, 36, 19, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'fangoterapia', NULL),
(20, 18, 'Pressoterapia', 37, 1, 38, 20, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'pressoterapia', NULL),
(21, 18, 'Peeling corpo', 39, 1, 40, 21, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'peeling-corpo', NULL),
(22, 18, 'Massaggio Circolatorio', 41, 1, 42, 22, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'massaggio-circolatorio', NULL),
(23, NULL, 'Trattamenti Viso', 44, 0, 53, 23, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'trattamenti-viso', NULL),
(24, 23, 'pulizia viso', 45, 1, 46, 24, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'pulizia-viso', NULL),
(25, 23, 'trattamento specifico', 47, 1, 48, 25, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'trattamento-specifico', NULL),
(26, 23, 'tratt. acido glicolico', 51, 1, 52, 26, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'tratt-acido-glicolico', NULL),
(27, 23, 'tratt. acido  ialuronico', 49, 1, 50, 27, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'tratt-acido-ialuronico', NULL),
(28, NULL, 'Medicina estetica', 54, 0, 65, 28, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'medicina-estetica', NULL),
(29, 28, 'Botulino viso', 55, 1, 56, 29, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'botulino-viso', NULL),
(30, 28, 'Rimodellamento labbra', 57, 1, 58, 30, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'rimodellamento-labbra', NULL),
(31, 28, 'Soft lifting con fili', 59, 1, 60, 31, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'soft-lifting-con-fili', NULL),
(32, 28, 'Rinofiller', 61, 1, 62, 32, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'rinofiller', NULL),
(33, 28, 'Peeling chimico', 63, 1, 64, 33, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'peeling-chimico', NULL),
(34, NULL, 'Luce Pulsata ed epilazione', 66, 0, 71, 34, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'luce-pulsata-ed-epilazione', NULL),
(35, 34, 'luce pulsata', 67, 1, 68, 35, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'luce-pulsata', NULL),
(36, 11, 'radiofrequenza', 29, 1, 30, 36, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'radiofrequenza-1', NULL),
(37, NULL, 'Trattamenti uomo', 72, 0, 81, 37, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'trattamenti-uomo', NULL),
(38, 37, 'Cera petto', 73, 1, 74, 38, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'cera-petto', NULL),
(39, 37, 'Cera gambe', 75, 1, 76, 39, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'cera-gambe', NULL),
(40, 37, 'Cera braccia', 77, 1, 78, 40, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'cera-braccia', NULL),
(41, 37, 'Cera schiena', 79, 1, 80, 41, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'cera-schiena', NULL),
(42, NULL, 'Igiene dentale', 82, 0, 93, 42, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'igiene-dentale', NULL),
(43, 42, 'Pulizia dentale', 83, 1, 84, 43, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'pulizia-dentale', NULL),
(44, 42, 'Sbiancamento dentale', 85, 1, 86, 44, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'sbiancamento-dentale', NULL),
(45, 42, 'Otturazioni estetiche', 87, 1, 88, 45, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'otturazioni-estetiche', NULL),
(46, 42, 'Ricostruzioni dentali estetiche', 89, 1, 90, 46, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'ricostruzioni-dentali-estetiche', NULL),
(47, 42, 'Applicazione di faccette estetiche', 91, 1, 92, 47, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'applicazione-di-faccette-estetiche', NULL),
(48, NULL, 'Manicure/Nail Art', 96, 0, 105, 48, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'manicure-nail-art', NULL),
(49, 54, 'Pedicure estetico', 107, 1, 108, 49, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'pedicure-estetico', NULL),
(50, 48, 'Manicure', 97, 1, 98, 50, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'manicure', NULL),
(51, 48, 'Manicure French', 99, 1, 100, 51, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'manicure-french', NULL),
(52, 48, 'Ricostruzione Gel', 101, 1, 102, 52, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'ricostruzione-gel', NULL),
(53, 48, 'Copertura Gel', 103, 1, 104, 53, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'copertura-gel', NULL),
(54, NULL, 'Pedicure estetico', 106, 0, 113, 54, 1, '2017-07-29 10:03:10', '2017-08-27 15:49:16', NULL, 'pedicure-estetico-1', NULL),
(55, 54, 'Tratt. paraffina piedi', 111, 1, 112, 55, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:38', NULL, 'tratt-paraffina-piedi', NULL),
(56, NULL, 'Podologia e Pedicure curativo', 114, 0, 123, 56, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:38', NULL, 'podologia-e-pedicure-curativo', NULL),
(57, 54, 'Pedicure curativo', 109, 1, 110, 57, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:31', NULL, 'pedicure-curativo', NULL),
(58, 56, 'Riflessologia plantare', 121, 1, 122, 58, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:16', NULL, 'riflessologia-plantare', NULL),
(59, 56, 'Baropodometria', 115, 1, 116, 59, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:16', NULL, 'baropodometria', NULL),
(60, 56, 'Onicocriptosi', 119, 1, 120, 60, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:16', NULL, 'onicocriptosi', NULL),
(61, 56, 'Orthoplastia', 117, 1, 118, 61, 1, '2017-07-29 10:03:10', '2017-08-02 12:41:16', NULL, 'orthoplastia', NULL),
(62, NULL, 'Trucco', 124, 0, 125, 62, 1, '2017-07-29 10:03:10', '2017-08-02 12:39:59', NULL, 'trucco', NULL),
(63, NULL, 'Parrucchiere', 126, 0, 127, 63, 1, '2017-07-29 10:03:10', '2017-08-02 12:39:59', NULL, 'parrucchiere', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `fos_group`
--

CREATE TABLE `fos_group` (
  `id` int(11) NOT NULL,
  `name` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `facebook_id`) VALUES
(1, 'guru', 'guru', 'a.gregoletto@maboncreative.com', 'a.gregoletto@maboncreative.com', 1, NULL, '$2y$13$iq8Xu1mxLtcNahjQrHi1WOUdzhkWq9nRvnDSY72/dfKknDEo/MOLq', '2017-09-23 10:03:07', NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `media__gallery`
--

CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `media__gallery`
--

INSERT INTO `media__gallery` (`id`, `name`, `context`, `default_format`, `enabled`, `updated_at`, `created_at`) VALUES
(5, 'Galleria Centro Venus', 'default', 'reference', 1, '2017-09-10 10:37:33', '2017-09-10 10:27:38');

-- --------------------------------------------------------

--
-- Struttura della tabella `media__gallery_media`
--

CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `media__gallery_media`
--

INSERT INTO `media__gallery_media` (`id`, `gallery_id`, `media_id`, `position`, `enabled`, `updated_at`, `created_at`) VALUES
(8, 5, 11, 1, 1, '2017-09-10 10:37:33', '2017-09-10 10:27:38');

-- --------------------------------------------------------

--
-- Struttura della tabella `media__media`
--

CREATE TABLE `media__media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_identifier` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `media__media`
--

INSERT INTO `media__media` (`id`, `name`, `description`, `enabled`, `provider_name`, `provider_status`, `provider_reference`, `provider_metadata`, `width`, `height`, `length`, `content_type`, `content_size`, `copyright`, `author_name`, `context`, `cdn_is_flushable`, `cdn_flush_identifier`, `cdn_flush_at`, `cdn_status`, `updated_at`, `created_at`) VALUES
(6, 'download.jpg', NULL, 1, 'sonata.media.provider.image', 1, '46a84866cc361f3c61150bfb118289506ca0e4e6.jpeg', '{"filename":"download.jpg"}', 225, 225, NULL, 'image/jpeg', 3695, NULL, NULL, 'default', NULL, NULL, NULL, NULL, '2017-09-09 17:49:25', '2017-09-09 17:49:25'),
(9, 'Centro Venus', NULL, 1, 'sonata.media.provider.image', 1, '676501c4b2be19fca82f96877520d2166500f4cc.jpeg', '{"filename":"popular-location-01.jpg"}', 800, 534, NULL, 'image/jpeg', 30799, NULL, NULL, 'default', 0, NULL, NULL, NULL, '2017-09-10 10:10:46', '2017-09-10 09:54:12'),
(10, 'category-box-01.jpg', NULL, 1, 'sonata.media.provider.image', 1, '80457a3a672df3b23537701be8b68a8aa0ab8773.jpeg', '{"filename":"category-box-01.jpg"}', 866, 569, NULL, 'image/jpeg', 105521, NULL, NULL, 'category', NULL, NULL, NULL, NULL, '2017-09-10 10:18:17', '2017-09-10 10:18:17'),
(11, 'single-listing-01.jpg', NULL, 1, 'sonata.media.provider.image', 1, '7cd9b2209ec880653e57ad24e008af4f0c2b7885.jpeg', '{"filename":"single-listing-01.jpg"}', 577, 866, NULL, 'image/jpeg', 67580, NULL, NULL, 'default', 0, NULL, NULL, NULL, '2017-09-10 10:38:59', '2017-09-10 10:27:25');

-- --------------------------------------------------------

--
-- Struttura della tabella `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  `lft` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `opening_hour`
--

CREATE TABLE `opening_hour` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `open_hour` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `close_hour` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `day` varchar(3) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `opening_hour`
--

INSERT INTO `opening_hour` (`id`, `store_id`, `open_hour`, `close_hour`, `day`) VALUES
(1, 1, '09:00', '17:00', 'mon'),
(2, 1, '09:00', '17:00', 'tue'),
(3, 1, '09:00', '19:00', 'wed'),
(4, 1, '09:00', '19:00', 'thu'),
(5, 1, '09:00', '17:00', 'fri'),
(6, 1, '09:00', '17:00', 'sat'),
(7, 1, '09:00', NULL, 'sun'),
(8, NULL, NULL, NULL, 'mon'),
(9, NULL, NULL, NULL, 'tue'),
(10, 25, NULL, NULL, 'mon'),
(11, 26, NULL, NULL, 'mon'),
(12, 27, NULL, NULL, 'mon');

-- --------------------------------------------------------

--
-- Struttura della tabella `social_network`
--

CREATE TABLE `social_network` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `social` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `social_network`
--

INSERT INTO `social_network` (`id`, `store_id`, `social`, `url`) VALUES
(2, 1, 'google-plus', 'http://google.com'),
(6, NULL, 'facebook', 'http://www.facebook.com'),
(7, NULL, 'google-plus', 'http://gplus.com'),
(8, 25, 'facebook', 'http://www.facebook.com'),
(9, 26, 'facebook', 'http://facebook.com'),
(10, 27, 'facebook', 'http://www.facebook.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(125) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `is_public` tinyint(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipology_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `store`
--

INSERT INTO `store` (`id`, `title`, `website`, `email`, `phone`, `address`, `latitude`, `longitude`, `description`, `is_public`, `created`, `updated`, `deletedAt`, `slug`, `city`, `tipology_id`, `user_id`, `image_id`, `gallery_id`) VALUES
(1, 'Centro Fenus', 'www.centro-estetico.it', 'a.gregoletto.mabon@gmail.com', '3454312039', 'Via Francesco Ladelci, Roma, RM, Italia', 41.8803289, 12.5660684, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur maximus laoreet. Phasellus lobortis turpis at suscipit consequat. Duis facilisis nulla eu risus ultrices pharetra. Phasellus laoreet, nisl vel ultrices sollicitudin, turpis ligula congue arcu, at eleifend felis lacus at nunc.</p>', 1, '2017-07-29 15:21:17', '2017-09-10 10:27:54', NULL, 'centro-estetico', 'Roma', 1, 1, 9, 5),
(2, 'Centro Estetico Malo', 'centroesteticomalo.com', '', '06 589 8950', 'Piazza Giuditta Tavani Arquati, 120, 00153 Roma RM', 41.8896324, 12.4730971, NULL, 1, '2017-07-29 15:21:18', '2017-07-29 15:21:18', NULL, 'centro-estetico-malo', 'Roma', 2, NULL, NULL, NULL),
(3, 'Thana Centro Estetico &amp;amp; Laser Center', 'thanaestetica.it', 'info@thanaestetica.it', '06 683 2035', 'Via di Monte Brianzo, 46, 00186 Roma RM', 41.9027353, 12.4744304, NULL, 1, '2017-07-29 15:21:18', '2017-08-23 10:16:29', NULL, 'thana-centro-estetico-amp-amp-laser-center', 'Roma', 2, NULL, NULL, NULL),
(4, 'Aesthetic Center Body Slim', 'body-slim.it', '', '06 780 0708', 'Via Eurialo, 2/a-b-c, 00181 Roma RM', 41.8751942, 12.524071, NULL, 1, '2017-07-29 15:21:20', '2017-07-29 15:21:20', NULL, 'aesthetic-center-body-slim', 'Roma', 1, NULL, NULL, NULL),
(5, 'Massaggio Estetica Orientale Iris', 'centroesteticoiris.it', 'xuelix@libero.it', '06 5728 9151', 'Via Pellegrino Matteucci, 88, 00154 Roma RM', 41.8715645, 12.482455, NULL, 1, '2017-07-29 15:21:20', '2017-07-29 15:21:20', NULL, 'massaggio-estetica-orientale-iris', 'Roma', 2, NULL, NULL, NULL),
(6, 'Aesthetic Center Rome - Les Soins', 'lessoins.com', 'info@lessoins.com', '06 8408 0077', 'Via Tarvisio, 4, 00198 Roma RM', 41.9220706, 12.5139845, NULL, 1, '2017-07-29 15:21:21', '2017-07-29 15:21:21', NULL, 'aesthetic-center-rome-les-soins', 'Roma', 2, NULL, NULL, NULL),
(7, 'Centro Benessere Saint Tropez Spa', 'sainttropezspa.it', '', '06 701 4900', 'Via Appia Nuova, 308/E/F, 00178 Roma RM', 41.8781688, 12.5182335, NULL, 1, '2017-07-29 15:21:22', '2017-07-29 15:21:22', NULL, 'centro-benessere-saint-tropez-spa', 'Roma', 2, NULL, NULL, NULL),
(8, 'Aesthetic Center Athena', 'athenaestetica.it', 'info@athenaestetica.it', '06 3751 4630', 'Viale Angelico, 4, 00195 Roma RM', 41.9117375, 12.4583896, NULL, 1, '2017-07-29 15:21:22', '2017-07-29 15:21:22', NULL, 'aesthetic-center-athena', 'Roma', 1, NULL, NULL, NULL),
(9, 'Estetica Caravaggio', 'esteticacaravaggio.it', 'info@esteticacaravaggio.it', '06 5960 1900', 'Via Accademia degli Agiati, 75, 00147 Roma RM', 41.8505052, 12.4908848, NULL, 1, '2017-07-29 15:21:23', '2017-07-29 15:21:23', NULL, 'estetica-caravaggio', 'Roma', 1, NULL, NULL, NULL),
(10, 'Myosotis Centro Estetico', 'esteticamyosotis.com', '', '06 8956 3286', 'Via Guido de Ruggiero, 30, 00142 Roma RM', 41.844224, 12.4814574, NULL, 1, '2017-07-29 15:21:23', '2017-07-29 15:21:23', NULL, 'myosotis-centro-estetico', 'Roma', 1, NULL, NULL, NULL),
(11, 'Wellness Center Myrea LTD', 'centrobenesseremyrea.it', 'info@centrobenesseremyrea.it', '06 4201 4463', 'Via Antonio Salandra, 8, 00187 Roma RM', 41.9056777, 12.4955706, NULL, 1, '2017-07-29 15:21:24', '2017-07-29 15:21:24', NULL, 'wellness-center-myrea-ltd', 'Roma', 1, NULL, NULL, NULL),
(12, 'Centro estetico e medico L\'orchidea', '', '', '06 4890 1259', 'Piazza dell\'Esquilino, 43, 00185 Roma RM', 41.8978569, 12.4993919, NULL, 1, '2017-07-29 15:21:24', '2017-07-29 15:21:24', NULL, 'centro-estetico-e-medico-lorchidea', 'Roma', 2, NULL, NULL, NULL),
(13, 'Aesthetic Center Rome jindian', 'centroesteticoroma.info', '', '06 9603 7166', 'Via dei Monti di Pietralata, 55, 00157 Roma RM', 41.9107191, 12.5360323, NULL, 1, '2017-07-29 15:21:25', '2017-07-29 15:21:25', NULL, 'aesthetic-center-rome-jindian', 'Roma', 1, NULL, NULL, NULL),
(14, 'Nika Centro Estetico E Benessere Di Fabiola Annunziata', '', '', '06 4287 1857', 'Via Belisario, 25, 00187 Roma RM', 41.9092032, 12.4981804, NULL, 1, '2017-07-29 15:21:25', '2017-07-29 15:21:25', NULL, 'nika-centro-estetico-e-benessere-di-fabiola-annunziata', 'Roma', 2, NULL, NULL, NULL),
(15, 'Aesthetic Center Solarium L.D. LTD', 'facebook.com', '', '06 275 2676', 'Via Renzo da Ceri, 126, 00177 Roma RM', 41.8889341, 12.5382732, NULL, 1, '2017-07-29 15:21:26', '2017-07-29 15:21:26', NULL, 'aesthetic-center-solarium-l-d-ltd', 'Roma', 1, NULL, NULL, NULL),
(16, 'Estetica solarium dimagrimento Ego &amp;amp; Bellezza', 'egobellezza.com', 'info@egobellezza.it', '06 323 4074', 'Via Orazio, 9, 00193 Roma RM', 41.9076963, 12.4662856, NULL, 1, '2017-07-29 15:21:26', '2017-07-29 15:21:26', NULL, 'estetica-solarium-dimagrimento-ego-amp-amp-bellezza', 'Roma', 2, NULL, NULL, NULL),
(17, 'Nail Spa Trastevere - Happy Sun Beauty', 'happysunbeauty.com', 'info@happysunbeauty.com,trastevere@happysunbeauty.com,torrino@happysunbeauty.com,monteverde@happysunbeauty.com', '06 5833 5481', 'Via Cesare Pascarella, 53, 00153 Roma RM', 41.8766756, 12.4674239, NULL, 1, '2017-07-29 15:21:27', '2017-07-29 15:21:27', NULL, 'nail-spa-trastevere-happy-sun-beauty', 'Roma', 1, NULL, NULL, NULL),
(18, 'Centro Estetico Roma - Seta Beauty San Giovanni', 'facebook.com', '', '06 8901 5592', 'Via Britannia, 43, 00183 Roma RM', 41.87895, 12.50835, NULL, 1, '2017-07-29 15:21:27', '2017-07-29 15:21:27', NULL, 'centro-estetico-roma-seta-beauty-san-giovanni', 'Roma', 2, NULL, NULL, NULL),
(19, 'Ray of Light Beauty Center, solarium, nail', '', '', '06 9684 5450', 'Via La Spezia, 26, 00182 Roma RM', 41.8859955, 12.5141228, NULL, 1, '2017-07-29 15:21:28', '2017-07-29 15:21:28', NULL, 'ray-of-light-beauty-center-solarium-nail', 'Roma', 2, NULL, NULL, NULL),
(20, 'Ikebana Centro Estetico - Benessere', 'ikebanabenessere.it', 'info@ikebanabenessere.it', '06 5195 6196', 'Via Paolo di Dono, 129, 00142 Roma RM', 41.8285128, 12.4941661, NULL, 1, '2017-07-29 15:21:28', '2017-07-29 15:21:28', NULL, 'ikebana-centro-estetico-benessere', 'Roma', 1, NULL, NULL, NULL),
(24, 'centro prova', NULL, 'a.gregoletto@maboncreative.com', NULL, 'Via dei Nani, Vicenza, VI, Italia', 45.5353094, 11.5567755, NULL, 1, '2017-09-08 13:45:33', '2017-09-08 13:45:33', NULL, 'centro-prova', 'Vicenza', 2, NULL, NULL, NULL),
(25, 'centro prova', NULL, 'a.gregoletto.mabon@gmail.com', '3454312039', 'Via delle Fornaci, Roma, RM, Italia', 41.8940825, 12.4575876, NULL, 1, '2017-09-08 13:55:47', '2017-09-08 13:55:47', '2017-09-08 15:09:24', 'centro-prova', 'Roma', 2, 1, NULL, NULL),
(26, 'Centro Fenus', NULL, 'a.gregoletto.mabon@gmail.com', NULL, 'Via del Corso, Roma, RM, Italia', 41.9034651, 12.4793991, NULL, 1, '2017-09-08 14:36:59', '2017-09-08 14:36:59', NULL, 'centro-fenus', 'Roma', 2, NULL, NULL, NULL),
(27, 'Centro Fenus', NULL, 'a.gregoletto.mabon@gmail.com', NULL, 'Via Derna, Castelnuovo del Garda, VR, Italia', 45.4471921, 10.7199185, NULL, 1, '2017-09-08 14:39:09', '2017-09-08 14:39:09', NULL, 'centro-fenus', 'Roma', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `stores_categories`
--

CREATE TABLE `stores_categories` (
  `store_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `stores_categories`
--

INSERT INTO `stores_categories` (`store_id`, `category_id`) VALUES
(1, 2),
(3, 5),
(26, 3),
(26, 4),
(27, 3),
(27, 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipology`
--

CREATE TABLE `tipology` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `tipology`
--

INSERT INTO `tipology` (`id`, `title`, `is_public`, `created`, `updated`, `deletedAt`, `slug`) VALUES
(1, 'Salone Di Bellezza', 1, '2017-08-28 11:05:13', '2017-08-28 11:05:13', NULL, 'salone-di-bellezza'),
(2, 'Parrucchiere', 1, '2017-08-28 11:05:26', '2017-08-28 11:05:26', NULL, 'parrucchiere');

-- --------------------------------------------------------

--
-- Struttura della tabella `treatment_price`
--

CREATE TABLE `treatment_price` (
  `id` int(11) NOT NULL,
  `treatment_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `reduce_price` decimal(10,2) DEFAULT NULL,
  `short_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `treatment_price`
--

INSERT INTO `treatment_price` (`id`, `treatment_id`, `store_id`, `price`, `reduce_price`, `short_description`) VALUES
(1, 2, NULL, '35.00', '30.00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur maximus laoreet. Phasellus lobortis turpis at suscipit consequat. Duis facilisis nulla eu risus ultrices pharetr'),
(2, 5, NULL, '48.00', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur maximus laoreet. Phasellus lobortis turpis at suscipit consequat. Duis facilisis nulla eu risus ultrices pharetr'),
(3, 2, NULL, '45.00', '40.00', 'deudheuhdeuhdeuhdueh du'),
(4, 2, NULL, '35.00', '30.00', '4feeefefef'),
(8, 1, NULL, '35.00', NULL, NULL),
(9, 3, NULL, '30.00', NULL, NULL),
(10, 1, 25, '35.00', NULL, NULL),
(11, 1, 26, '43.00', NULL, NULL),
(12, 1, 27, '45.00', NULL, NULL),
(13, 2, 1, '34.00', NULL, NULL);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_64C19C1727ACA70` (`parent_id`),
  ADD KEY `IDX_64C19C13DA5256D` (`image_id`);

--
-- Indici per le tabelle `fos_group`
--
ALTER TABLE `fos_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indici per le tabelle `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Indici per le tabelle `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indici per le tabelle `media__gallery`
--
ALTER TABLE `media__gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  ADD KEY `IDX_80D4C541EA9FDD75` (`media_id`);

--
-- Indici per le tabelle `media__media`
--
ALTER TABLE `media__media`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7D053A93727ACA70` (`parent_id`);

--
-- Indici per le tabelle `opening_hour`
--
ALTER TABLE `opening_hour`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_969BD765B092A811` (`store_id`);

--
-- Indici per le tabelle `social_network`
--
ALTER TABLE `social_network`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EFFF5221B092A811` (`store_id`);

--
-- Indici per le tabelle `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`),
  ADD KEY `search_idx` (`address`,`city`,`latitude`,`longitude`),
  ADD KEY `IDX_FF575877E0770D92` (`tipology_id`),
  ADD KEY `IDX_FF575877A76ED395` (`user_id`),
  ADD KEY `IDX_FF5758773DA5256D` (`image_id`),
  ADD KEY `IDX_FF5758774E7AF8F` (`gallery_id`);

--
-- Indici per le tabelle `stores_categories`
--
ALTER TABLE `stores_categories`
  ADD PRIMARY KEY (`store_id`,`category_id`),
  ADD KEY `IDX_9A1657F7B092A811` (`store_id`),
  ADD KEY `IDX_9A1657F712469DE2` (`category_id`);

--
-- Indici per le tabelle `tipology`
--
ALTER TABLE `tipology`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `treatment_price`
--
ALTER TABLE `treatment_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A0BBC1E7471C0366` (`treatment_id`),
  ADD KEY `IDX_A0BBC1E7B092A811` (`store_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT per la tabella `fos_group`
--
ALTER TABLE `fos_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `media__gallery`
--
ALTER TABLE `media__gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT per la tabella `media__media`
--
ALTER TABLE `media__media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT per la tabella `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `opening_hour`
--
ALTER TABLE `opening_hour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT per la tabella `social_network`
--
ALTER TABLE `social_network`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT per la tabella `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT per la tabella `tipology`
--
ALTER TABLE `tipology`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `treatment_price`
--
ALTER TABLE `treatment_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `FK_64C19C13DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_64C19C1727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`) ON DELETE SET NULL;

--
-- Limiti per la tabella `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`);

--
-- Limiti per la tabella `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Limiti per la tabella `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_7D053A93727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`) ON DELETE SET NULL;

--
-- Limiti per la tabella `opening_hour`
--
ALTER TABLE `opening_hour`
  ADD CONSTRAINT `FK_969BD765B092A811` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`);

--
-- Limiti per la tabella `social_network`
--
ALTER TABLE `social_network`
  ADD CONSTRAINT `FK_EFFF5221B092A811` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`);

--
-- Limiti per la tabella `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `FK_FF5758773DA5256D` FOREIGN KEY (`image_id`) REFERENCES `media__media` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_FF5758774E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_FF575877A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_FF575877E0770D92` FOREIGN KEY (`tipology_id`) REFERENCES `tipology` (`id`);

--
-- Limiti per la tabella `stores_categories`
--
ALTER TABLE `stores_categories`
  ADD CONSTRAINT `FK_9A1657F712469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_9A1657F7B092A811` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`);

--
-- Limiti per la tabella `treatment_price`
--
ALTER TABLE `treatment_price`
  ADD CONSTRAINT `FK_A0BBC1E7471C0366` FOREIGN KEY (`treatment_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_A0BBC1E7B092A811` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
