<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OpeningHourAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('day', ChoiceType::class, array(
                'label' => 'app.openingHour.form.day',
                'choices' => [
                    'Monday' => 'mon',
                    'Tuesday' => 'tue',
                    'Wednesday' => 'wed',
                    'Thursday' => 'thu',
                    'Friday' => 'fri',
                    'Saturday' => 'sat',
                    'Sunday' => 'sun'
                ]
            ))
            ->add('openHour', null, array(
                'label' => 'app.openingHour.form.openHour',
            ))
            ->add('closeHour', null, array(
                'label' => 'app.openingHour.form.closeHour',
            ));
    }
}