<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TipologyAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array(
                'label' => 'app.form.title'
            ))
            ->add('isPublic', null, array(
                'label' => 'app.form.isPublic'
            ))
            ->add('_action', 'actions', array(
                'label' => 'app.form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                    'view' => array()

                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'app.form.title'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, array(
                'label' => 'app.form.title'
            ))
            ->add('isPublic', null, array(
                'label' => 'app.form.isPublic'
            ));

    }
}