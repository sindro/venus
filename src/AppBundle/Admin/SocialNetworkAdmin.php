<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SocialNetworkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('social', ChoiceType::class, array(
                'label' => 'app.socialNetworks.form.social',
                'choices' => [
                    'Facebook' => 'facebook',
                    'Google Plus' => 'google-plus',
                    'Twitter' => 'twitter',
                    'Pinterest' => 'pinterest',
                    'Instagram' => 'instagram'
                ]
            ))
            ->add('url', null, array(
                'label' => 'app.form.url',
            ));
    }
}