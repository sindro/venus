<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CategoryAdmin extends AbstractAdmin
{
    public function configureRoutes(RouteCollection $routes)
    {
        $routes->add('tree', 'tree');
        $routes->add('jsonTree', 'json/tree');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array(
                'label' => 'app.form.title'
            ))
            ->add('isPublic', null, array(
                'label' => 'app.form.isPublic',
                'editable' => true
            ))
            ->add('_action', 'actions', array(
                'label' => 'app.form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                    
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'app.form.title'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {        
        $formMapper
            ->add('title', null, array(
                'label' => 'app.form.title'
            ))
            ->add('image', 'sonata_type_model_list', array(
                    'label' => 'Viene visualizzata negli eventi presenti in homepage (360x150)',
                    'required' => false
                ), 
                array(
                    'link_parameters' => array(
                    'context' => 'category',
                    'hide_context' => true,
                ),
            ))
            ->add('parent', 'sonata_type_model_list', array(
                'label' => 'app.category.form.parent',
                'required' => false,
                'btn_add' => false
            ))
            ->add('isPublic', null, array(
                'label' => 'app.form.isPublic'
            ));
    }

    public function delete($object)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        if($object->hasChildren()) {
            foreach ($object->getChildren() as $child) {
                $child->setParent(NULL);
                $em->persist($child);
                $em->flush();
            }
        }

        $this->preRemove($object);
        $this->getSecurityHandler()->deleteObjectSecurity($this, $object);
        $this->getModelManager()->delete($object);
        $this->postRemove($object);
    }

    public function toString($object)
    {
        return $object->getTitle();
    }
}