<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class TreatmentPriceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('treatment', 'sonata_type_model', array(
                    'class' => 'AppBundle:Category',
                    'multiple' => false,
                    'expanded' => false,
                    'required' => false,
                    'label' => 'app.treatmentPrice.form.treatment'
                ))
            ->add('shortDescription', null, array(
                'label' => 'app.treatmentPrice.form.shortDescription',
            ))
            ->add('price', MoneyType::class, array(
                'label' => 'app.treatmentPrice.form.price'
            ))
            ->add('reducePrice', MoneyType::class, array(
                'label' => 'app.treatmentPrice.form.reduce_price'
            ));
    }
}