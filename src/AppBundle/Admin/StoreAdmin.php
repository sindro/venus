<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class StoreAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array(
                'label' => 'app.form.title'
            ))
            ->add('categories', null, array(
                'label' => 'app.form.categories'
            ))
            ->add('tipology', null, array(
                'label' => 'app.store.form.tipology'
            ))
            ->add('isPublic', null, array(
                'label' => 'app.form.isPublic',
                'editable' => true
            ))
            ->add('_action', 'actions', array(
                'label' => 'app.form.actions',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                    
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array(
                'label' => 'app.form.title'
            ))
            ->add('categories', null, array(
                'label' => 'app.form.categories'
            ))
            ->add('tipology', null, array(
                'label' => 'app.store.form.tipology'
            ));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {    
        $categories = $this->getConfigurationPool()->getContainer()->get('app.entity_repository.category')->findAll();
        
        $formMapper
            ->with('Centro', array(
                'class'       => 'col-md-10',
                'box_class'   => 'box box-primary'
            ))
                ->add('title', null, array(
                    'label' => 'app.form.title'
                ))
                ->add('image', 'sonata_type_model_list', array(
                        'label' => 'Viene visualizzata negli eventi presenti in homepage (360x150)',
                        'required' => false
                    ), 
                    array(
                        'link_parameters' => array(
                        'context' => 'default',
                        'hide_context' => true,
                    ),
                ))
                ->add('email', null, array(
                    'label' => 'app.form.email'
                ))
                ->add('website', null, array(
                    'label' => 'app.form.website'
                ))
                ->add('phone', null, array(
                    'label' => 'app.form.phone'
                ))
                ->add('address', null, array(
                    'label' => 'app.form.address'
                ))
                ->add('city', null, array(
                    'label' => 'app.form.city'
                ))
                ->add('description', 'sonata_simple_formatter_type', array(
                    'format' => 'richhtml',
                    'ckeditor_context' => 'administrator',
                    'label' => 'app.form.description'
                ))
                ->add('treatmentPrices', 'sonata_type_collection', array(
                    'label' => 'app.store.form.treatmentPrices',
                    ), array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'limit' => count($categories)
                    )
                )
                ->add('openingHours', 'sonata_type_collection', array(
                    'label' => 'app.store.form.openingHours',
                    ), array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'limit' => 7
                    )
                )
                ->add('socialNetworks', 'sonata_type_collection', array(
                    'label' => 'app.store.form.socialNetworks',
                    ), array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'limit' => 5
                    )
                )
                ->add('gallery', 'sonata_type_model_list', array(
                    'label' => 'app.store.form.gallery',
                    'required' => false
                ))
            ->end()
            ->with('Generali', array(
                'class'       => 'col-md-2',
                'box_class'   => 'box box-primary'
            ))
                ->add('user', null, array(
                    'label' => 'app.user.form'
                ))
                ->add('tipology', 'sonata_type_model', array(
                    'class' => 'AppBundle:Tipology',
                    'multiple' => false,
                    'expanded' => false,
                    'required' => false,
                    'label' => 'app.store.form.tipology'
                ))
                ->add('categories', 'sonata_type_model', array(
                    'class' => 'AppBundle:Category',
                    'multiple' => true,
                    'expanded' => false,
                    'required' => false,
                    'label' => 'app.form.categories'
                ))
                ->add('isPublic', null, array(
                    'label' => 'app.form.isPublic'
                ))
            ->end();
    }

    public function prePersist($object)
    {
        foreach ($object->getOpeningHours() as $oh) {
            $oh->setStore($object);
        }
        
        foreach ($object->getSocialNetworks() as $sn) {
            $sn->setStore($object);
        }
        
        foreach ($object->getTreatmentPrices() as $tp) {
            $tp->setStore($object);
        }
    }

    public function preUpdate($object)
    {       
        foreach ($object->getOpeningHours() as $oh) {
            $oh->setStore($object);
        }
        
        foreach ($object->getSocialNetworks() as $sn) {
            $sn->setStore($object);
        }
        
        foreach ($object->getTreatmentPrices() as $tp) {
            $tp->setStore($object);
        }
    }
    
    public function toString($object)
    {
        return $object->getTitle();
    }
}