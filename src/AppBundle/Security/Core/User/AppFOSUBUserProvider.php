<?php

namespace AppBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Entity\User;

class AppFOSUBUserProvider extends BaseFOSUBProvider
{
    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        
        $username = $response->getUsername();
        //on connect - get the access token and the user ID
        $serviceName = $response->getResourceOwner()->getName();
        $setter_id = 'set' . ucfirst($serviceName) .'Id';
        $setter_token = 'set' . ucfirst($serviceName) . 'AccessToken';
        
        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }
        
        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        
        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $userEmail = $response->getEmail();
        $user = $this->userManager->findUserByEmail($userEmail);

        $serviceName = $response->getResourceOwner()->getName();
        $setter_id = 'set' . ucfirst($serviceName) .'Id';
        $setter_token = 'set' . ucfirst($serviceName) . 'AccessToken';
            
        if (null === $user) {
            $username = $response->getUsername();

            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());
            $user->setUsername($username);
            $user->setEmail($userEmail);
            $user->setPlainPassword($username);
            $user->setEnabled(true);
            $this->userManager->updateUser($user);

            return $user;
        }
        
        // else update access token of existing user
        $user->$setter_token($response->getAccessToken());//update access token

        return $user;
    }

}