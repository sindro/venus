<?php
namespace AppBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use AppBundle\Util\Geocoder;

class ResponseListener
{
    protected $geocoder;

    public function __construct(Geocoder $geocoder)
    {
        $this->geocoder = $geocoder;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response  = $event->getResponse();
        $request = $event->getRequest();
        
        if(!$request->cookies->has('locality'))
        {
            $coords = $this->geocoder->getCoordinates(null);

            $locality = $this->geocoder->getLocality($coords->getLatitude(), $coords->getLongitude());

            $response->headers->set('set-cookie', 'locality=' . $locality, false);
        }
        
        
    }
}
