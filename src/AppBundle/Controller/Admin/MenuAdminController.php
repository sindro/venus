<?php
namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;

class MenuAdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request = null)
    {
        /*if (!$request->get('filter') && !$request->get('filters')) {
            return new RedirectResponse($this->admin->generateUrl('tree'));
        }*/

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();

        if ($this->admin->getPersistentParameter('context')) {
            $datagrid->setValue('context', null, $this->admin->getPersistentParameter('context'));
        }

        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render('AppBundle:Admin/Menu:list.html.twig', array(
            'action'     => 'list',
            'form'       => $formView,
            'datagrid'   => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function treeAction(Request $request)
    {
        $links = $this->get('app.entity_repository.menu')->findRootNodes(false);

        $datagrid = $this->admin->getDatagrid();

        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render('AppBundle:Admin/Menu:tree.html.twig', array(
            'action' => 'tree',
            'links' => $links,
            'form' => $formView,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    public function jsonTreeAction(Request $request)
    {
        $json = $this->cleanJson($request->get('json'));

        if (is_array($json)) {
            for ($i = 1; $i < count($json['data']); $i++) {
                $repository = $this->get('app.entity_repository.menu');
                $em = $this->container->get('doctrine')->getManager();

                $object = $repository->findOneById($json['data'][$i]['id']);

                $object->setLft($json['data'][$i]['left']);
                $object->setRgt($json['data'][$i]['right']);
                $object->setLvl($json['data'][$i]['depth']);

                if (isset($json['data'][$i]['parent_id'])) {
                    $parent = $repository->findOneById($json['data'][$i]['parent_id']);
                    $object->setParent($parent);
                } else {
                    $object->setParent(null);
                }

                $em->persist($object);
                $em->flush();
            }

            echo json_encode(array(
                'type' => 'success',
                'message' => $this->get('translator')->trans('app.message.flash.order.success')
            ));
            exit();
        }

        echo json_encode(array(
            'type' => 'error',
            'message' => $this->get('translator')->trans('app.message.flash.order.error')
        ));
        exit();
    }

    private function cleanJson($data)
    {
        $create_json_1 = str_replace(',}', '}', $data);
        $create_json_2 = str_replace(',]', ']', $create_json_1);
        $create_json_3 = str_replace('{},', "", $create_json_2);
        $create_json_4 = str_replace('none', '"none"', $create_json_3);

        return json_decode($create_json_4, true);
    }
}