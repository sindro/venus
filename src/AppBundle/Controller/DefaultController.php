<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function indexAction(Request $request)
    {
        $coords = $this->get('app.geocoder')->getCoordinates(null);

        $locality = $this->get('app.geocoder')->getLocality($coords->getLatitude(), $coords->getLongitude());
        
        $categories = $this->get('app.entity_repository.category')->findRootNodes();
        
        return $this->render('default/index.html.twig', [
            'categories' => $categories,
            'locality' => $locality
        ]);
    }
    
    /**
     * @Route("/_menu/navigation", name="app_menu")
     */
    public function _menuAction(Request $request)
    {
        $menu = $this->get('app.entity_repository.menu')->findRootNodes();
        
        return $this->render('default/_menu.html.twig', [
            'menu' => $menu
        ]);
    }
    
    /**
     * @Route("/_sidebar/form", name="app_sidebar")
     */
    public function _sidebarAction(Request $request)
    {
        $categories = $this->get('app.entity_repository.category')->findRootNodes();
        $tipologies = $this->get('app.entity_repository.tipology')->findAll();
        
        return $this->render('default/_sidebar.html.twig', [
            'categories' => $categories,
            'tipologies' => $tipologies,
            'request' => $request
        ]);
    }
}
