<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StoreController extends Controller
{    
    /**
     * @Route("/{category}", name="app_stores")
     */
    public function indexAction(Request $request, $category)
    {
        $coords = $this->get('app.geocoder')->getCoordinates(null);

        $paginator = $this->get('knp_paginator');
        
        $stores = $this->get('app.entity_repository.store')->findByDistance(50, $coords->getLatitude(), $coords->getLongitude(), $category);

        $pagination = $paginator->paginate($stores, $request->query->getInt('page', 1), 10);
        
        $ajax_url = $this->generateUrl('app_stores', ['category' => $category]);
        
        return $this->render('store/index.html.twig', [
            'pagination' => $pagination,
            'ajax_url' => $ajax_url
        ]);
    }
    
    /**
     * @Route("/{category}/{city}", name="app_city_stores")
     */
    public function cityAction(Request $request, $category, $city)
    {
        $coords = $this->get('app.geocoder')->getCoordinates($city);
        
        $paginator = $this->get('knp_paginator');
        
        $stores = $this->get('app.entity_repository.store')->findByDistance(50, $coords->getLatitude(), $coords->getLongitude(), $category);

        $pagination = $paginator->paginate($stores, $request->query->getInt('page', 1), 10);
        
        $ajax_url = $this->generateUrl('app_city_stores', ['category' => $category, 'city' => $city]);
        
        return $this->render('store/index.html.twig', [
            'pagination' => $pagination,
            'ajax_url' => $ajax_url
        ]);
    }
   
    /**
     * @Route("/{tipology}/{slug}/{city}", name="app_store_show")
     */
    public function showAction(Request $request, $tipology, $slug, $city)
    {
        $store = $this->get('app.entity_repository.store')->findOneBySlug($slug);
        
        if(!$store)
        {
            throw new NotFoundHttpException('Store not found');
        }
        
        return $this->render('store/show.html.twig', [
            'store' => $store
        ]);
    }
}
