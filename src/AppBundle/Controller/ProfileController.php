<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Form\Type\StoreType;
use AppBundle\Entity\Store;

class ProfileController extends BaseController
{ 
    /**
     * @Route("/profile/store/list", name="app_profile_store_list")
     */
    public function listStoreAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
        $paginator = $this->get('knp_paginator');
        
        $stores = $this->get('app.entity_repository.store')->findBy(['user' => $user]);
        
        $tipologies = [];
        
        foreach($stores as $store)
        {
            $tipologies[] = $store->getTipology()->getId();
        }

        $pagination = $paginator->paginate($stores, $request->query->getInt('page', 1), 10);
        
        return $this->render('AppBundle:Profile/Store:list.html.twig', ['pagination' => $pagination]);
    }
    
    /**
     * @Route("/profile/store/edit/{id}", name="app_profile_store_edit")
     */
    public function editStoreAction(Request $request, $id)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
        $admin = $this->get('admin.store');
        
        $object = $admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException('No store found for id '. $request->get('id'));
        }
        
        $form = $this->createForm(StoreType::class, $object);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $submittedObject = $form->getData();
                $admin->setSubject($submittedObject);
                
                $admin->update($submittedObject);
                
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app.message.flash.store.edit_success'));
                
                return new RedirectResponse($this->generateUrl('app_profile_store_edit', ['id' => $id]));
            }
            
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app.message.flash.store.error'));
            
        }
        
        return $this->render('AppBundle:Profile/Store:edit.html.twig', ['form' => $form->createView(), 'store' => $object]);
    }
    
    /**
     * @Route("/profile/store/add", name="app_profile_store_add")
     */
    public function addStoreAction(Request $request)
    {
        $user = $this->getUser();
        
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
        $stores = $this->get('app.entity_repository.store')->countNumberStoresForUser($user);
        
        if($stores >= $this->getParameter('max_stores_per_user'))
        {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
        $admin = $this->get('admin.store');

        $newObject = $admin->getNewInstance();
        
        $form = $this->createForm(StoreType::class, $newObject);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $submittedObject = $form->getData();
                $admin->setSubject($submittedObject);
                
                $newObject = $admin->create($submittedObject);
                
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app.message.flash.store.add_success'));
                
                return new RedirectResponse($this->generateUrl('app_profile_store_edit', ['id' => $newObject->getId()]));
            }
            
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('app.message.flash.store.error'));
            
        }
        
        return $this->render('AppBundle:Profile/Store:add.html.twig', ['form' => $form->createView()]);
    }
    
    /**
     * @Route("/profile/store/delete/{id}", name="app_profile_store_delete")
     */
    public function deleteStoreAction(Request $request, $id)
    {
        $user = $this->getUser();
        
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
        $admin = $this->get('admin.store');
        
        $object = $admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException('No store found for id '. $request->get('id'));
        }
        
        $admin->delete($object);
        
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('app.message.flash.store.delete_success'));
        
        return new RedirectResponse($this->generateUrl('app_profile_store_list'));
    }
}
