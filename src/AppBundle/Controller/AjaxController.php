<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class AjaxController extends Controller
{
    /**
     * @Route("/marker/clusterer/google/maps/popup", name="app_ajax_google_maps")
     */
    public function googleMapsAction(Request $request)
    {
        $coords = $this->get('app.geocoder')->getCoordinates($request->get('dove'));
        
        $stores = $this->get('app.entity_repository.store')->findByDistance(
                    $request->get('raggio') ? $request->get('raggio') : 50, 
                    $coords->getLatitude(), 
                    $coords->getLongitude(), 
                    $request->get('categoria'), 
                    $request->get('tipologia')
                );
        
        if($request->isXmlHttpRequest()) 
        {
            return $this->render('ajax/stores.json.twig', [
                'stores' => $stores
            ]);
        }
  
        $query = array_filter($request->query->all());
        
        unset($query['page']);

        return $this->render('store/google_maps.html.twig', [
            'stores' => $stores,
            'coords' => $coords,
            'query' => $query
        ]);
    }
}
