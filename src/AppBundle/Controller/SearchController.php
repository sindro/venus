<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Geocoder\Query\GeocodeQuery;

class SearchController extends Controller
{
    /**
     * @Route("/ricerca", name="app_search")
     */
    public function searchAction(Request $request)
    {
        $place = $request->get('dove');
        $radius = $request->get('raggio') ? $request->get('raggio') : 50;
        $tipology = $request->get('tipologia');
        $category = $request->get('categoria');
        
        $coords = $this->get('app.geocoder')->getCoordinates($place);
        
        $paginator = $this->get('knp_paginator');
        
        $stores = $this->get('app.entity_repository.store')->findByDistance(
                $radius, $coords->getLatitude(), $coords->getLongitude(), $category, $tipology);
  
        $pagination = $paginator->paginate($stores, $request->get('page') ? $request->get('page') : 1, 10);
        
        $query = array_filter($request->query->all());
        
        unset($query['page']);

        return $this->render('search/results.html.twig', [
            'pagination' => $pagination,
            'query' => $query
        ]);
    }
    
    /**
     * @Route("/_search/form", name="app_search_categories")
     */
    public function _searchAction(Request $request, $custom_css)
    {
        $categories = $this->get('app.entity_repository.category')->findRootNodes();
        
        return $this->render('search/_search.html.twig', [
            'categories' => $categories,
            'custom_css' => $custom_css,
        ]);
    }
    
    private function getCoordinates($request, $city = null)
    {
        $query = $city ? $city : $request->server->get('REMOTE_ADDR');
        $provider = $city ? 'app_google_maps' : 'app_geo_plugin';
        
        $result = $this->container
            ->get('bazinga_geocoder.provider.' . $provider)
            ->geocodeQuery(GeocodeQuery::create($query));
        
        return $result->first()->getCoordinates();
    }
}
