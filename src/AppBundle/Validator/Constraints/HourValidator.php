<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
/**
 * @Annotation
 */
class HourValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value != null && !preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}