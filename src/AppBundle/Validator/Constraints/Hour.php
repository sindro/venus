<?php
namespace AppBundle\Validator\Constraints;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
*/
class Hour extends Constraint
{
    public $message = 'Il formato {{ string }} inserito non è valido.';
    
    public function validatedBy()
    {
        return HourValidator::class;
    }
}