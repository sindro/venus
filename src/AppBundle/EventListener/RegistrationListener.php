<?php

namespace AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use AppBundle\Util\FormErrors;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\GroupManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
//use AppBundle\Entity\Newsletter;

class RegistrationListener implements EventSubscriberInterface
{
    private $formErrors;
    private $manager;
    private $groupManager;
    private $tokenStorage;
    
    public function __construct(FormErrors $formErrors, ObjectManager $manager, GroupManagerInterface $groupManager, TokenStorageInterface $tokenStorage) {
        $this->formErrors = $formErrors;
        $this->manager = $manager;
        $this->groupManager = $groupManager;
        $this->tokenStorage = $tokenStorage;
    }
    
    public static function getSubscribedEvents()
    {
        return array(
           // FOSUserEvents::REGISTRATION_SUCCESS => 'registrationSuccess',
            FOSUserEvents::REGISTRATION_COMPLETED => 'registrationCompleted',
            FOSUserEvents::REGISTRATION_FAILURE => 'registrationFailed',
        );
    }
    /*
    public function registrationSuccess(FormEvent $event)
    {
        $user = $event->getForm()->getData();
        $article = $this->manager->getRepository('AppBundle:Article')->findOneBySlug($event->getForm()->get("article")->getData());
        
        
        if($article) {
            $group = $this->groupManager->findGroupByName('Associato');
            $user->addGroup($group);
        }
         
        $emailNewsletter = $this->manager->getRepository('AppBundle:Newsletter')->findOneBy(['email' => $user->getEmail()]);
         
         if($event->getForm()['newsletter']->getData() && !$emailNewsletter) {
            $newsletter = new Newsletter();
            $newsletter->setEmail($user->getEmail());
            $this->manager->persist($newsletter);
            $this->manager->flush();
        }
    }*/

    public function registrationCompleted(FilterUserResponseEvent $event) 
    {       
        $token = new UsernamePasswordToken($event->getUser(), null, 'main', $event->getUser()->getRoles());
        $this->tokenStorage->setToken($token);
        $event->getRequest()->getSession()->set('_security_main', serialize($token));
        
        if($event->getRequest()->isXmlHttpRequest()) {
            echo json_encode(array(
                'status' => 'success',
                'redirect' => $event->getRequest()->server->get('HTTP_REFERER'),
            ));
            exit();
        }
    }
    
    public function registrationFailed(FormEvent $event) 
    {
        if($event->getRequest()->isXmlHttpRequest()) {
            echo json_encode(array(
                'status' => 'danger',
                'message' => $event->getRequest()->getSession()->getFlashBag()->get('danger'),
                'errors' => $this->formErrors->getArray($event->getForm())
            ));
            exit();
        }
    }
}
