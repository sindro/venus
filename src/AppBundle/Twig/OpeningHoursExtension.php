<?php
namespace AppBundle\Twig;

use AppBundle\Util\StoreHours;
use Doctrine\ORM\PersistentCollection;

class OpeningHoursExtension extends \Twig_Extension
{
    private $config;
    
    public function getFunctions()
    {
        return [
            new \Twig_Function('oh_table', [$this, 'getOhTable'])
        ];
    }
    
    public function __construct()
    {
        date_default_timezone_set('Europe/Rome');
       
        $this->config = [
            'separator'      => ' - ',
            'join'           => ' and ',
            'format'         => 'g:ia',
            'overview_weekdays'  => ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        ];

    }
    
    public function getOhTable(PersistentCollection $openingHours)
    {
        $hours = [];
        
        foreach($openingHours as $openingHour) {
            $hours[$openingHour->getDay()] = $openingHour->getOpenHour() && $openingHour->getCloseHour() ? [$openingHour->getOpenHour() . '-' . $openingHour->getCloseHour()] : [''];
        }

        $store_hours = new StoreHours($hours, [], $this->config);
        
        return ['hour_this_weeks' => $store_hours->hours_this_week(), 'is_open' => $store_hours->is_open()];
    }
}
