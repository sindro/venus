<?php
namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="treatment_price")
 */
class TreatmentPrice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", cascade={"persist"})
     */
    private $treatment;
    
    /**
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=true)
     * @Assert\NotBlank()
     */
    private $price;
    
    /**
     * @ORM\Column(name="reduce_price", type="decimal", precision=10, scale=2, nullable=true)
     * @Assert\Expression(
     *     "not (this.getReducePrice() > this.getPrice())",
     *     message="Value must be less then regular price"
     * )
     */
    private $reducePrice;
    
    /**
     * @ORM\Column(name="short_description", type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Store", inversedBy="treatmentPrices", cascade={"persist"})
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * */
    private $store;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return TreatmentPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set reducePrice
     *
     * @param string $reducePrice
     *
     * @return TreatmentPrice
     */
    public function setReducePrice($reducePrice)
    {
        $this->reducePrice = $reducePrice;

        return $this;
    }

    /**
     * Get reducePrice
     *
     * @return string
     */
    public function getReducePrice()
    {
        return $this->reducePrice;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return TreatmentPrice
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set treatment
     *
     * @param \AppBundle\Entity\Category $treatment
     *
     * @return TreatmentPrice
     */
    public function setTreatment(\AppBundle\Entity\Category $treatment = null)
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * Get treatment
     *
     * @return \AppBundle\Entity\Category
     */
    public function getTreatment()
    {
        return $this->treatment;
    }

    /**
     * Set store
     *
     * @param \AppBundle\Entity\Store $store
     *
     * @return TreatmentPrice
     */
    public function setStore(\AppBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \AppBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
    
    public function __toString()
    {
        return (String) $this->getTreatment();
    }
}
