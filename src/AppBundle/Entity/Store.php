<?php
namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Bazinga\GeocoderBundle\Mapping\Annotations as Geocoder;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="store",indexes={@ORM\Index(name="search_idx", columns={"address", "city", "latitude", "longitude"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\StoreRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @Geocoder\Geocodeable
 */
class Store {
    
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", fetch="EAGER")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $image;
    
    /**
     * @ORM\ManyToMany(targetEntity="Category", cascade={"persist"}, inversedBy="stores")
     * @ORM\JoinTable(name="stores_categories",
     *      joinColumns={@ORM\JoinColumn(name="store_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     * @Assert\Valid
     * @Assert\NotBlank()
     */
    private $categories;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tipology", cascade={"persist"})
     * @Assert\NotBlank()
     */
    private $tipology;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TreatmentPrice", mappedBy="store", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid
     */
    private $treatmentPrices;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OpeningHour", mappedBy="store", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid
     */
    private $openingHours;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SocialNetwork", mappedBy="store", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid
     */
    private $socialNetworks;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;
    
    /**
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;
    
    /**
     * @ORM\Column(name="email", type="string", length=125, nullable=true)
     * @Assert\NotBlank()
     */
    private $email;
    
    /**
     * @ORM\Column(name="phone", type="string", length=64, nullable=true)
     */
    private $phone;

    /**
     * @Geocoder\Address
     * @ORM\Column(name="address", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $address;
    
    /**
     * @ORM\Column(name="city", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $city;
    
    /**
     * @Geocoder\Latitude
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;
    
    /**
     * @Geocoder\Longitude
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;
    
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"all"})
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery")
     */
    private $gallery;
    
    /**
     * @ORM\Column(name="is_public", type="boolean", nullable=true)
     */
    private $isPublic = true;
    
    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;
    
    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;
    
    /**
     * @Gedmo\Slug(fields={"title"}, updatable=false, unique=false)
     * @ORM\Column(length=128, unique=false)
     */
    private $slug;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->openingHours = new \Doctrine\Common\Collections\ArrayCollection();
        $this->treatmentPrices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->socialNetworks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return Store
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Store
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Store
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Store
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Store
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Store
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Store
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Store
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Store
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Store
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return User
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set gallery
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery $gallery
     *
     * @return Store
     */
    public function setGallery(\Application\Sonata\MediaBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }
    
    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return Store
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Store
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Store
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Store
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Store
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Store
     */
    public function addCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \AppBundle\Entity\Category $category
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set tipology
     *
     * @param \AppBundle\Entity\Tipology $tipology
     *
     * @return Store
     */
    public function setTipology(\AppBundle\Entity\Tipology $tipology = null)
    {
        $this->tipology = $tipology;

        return $this;
    }

    /**
     * Get tipology
     *
     * @return \AppBundle\Entity\Tipology
     */
    public function getTipology()
    {
        return $this->tipology;
    }
    
    /**
     * Add treatmentPrice
     *
     * @param \AppBundle\Entity\TreatmentPrice $treatmentPrice
     *
     * @return Store
     */
    public function addTreatmentPrice(\AppBundle\Entity\TreatmentPrice $treatmentPrice)
    {
        $this->treatmentPrices[] = $treatmentPrice;

        return $this;
    }

    /**
     * Remove treatmentPrice
     *
     * @param \AppBundle\Entity\TreatmentPrice $treatmentPrice
     */
    public function removeTreatmentPrice(\AppBundle\Entity\TreatmentPrice $treatmentPrice)
    {
        $this->treatmentPrices->removeElement($treatmentPrice);
    }

    /**
     * Get treatmentPrices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTreatmentPrices()
    {
        return $this->treatmentPrices;
    }

    /**
     * Add openingHour
     *
     * @param \AppBundle\Entity\OpeningHour $openingHour
     *
     * @return Store
     */
    public function addOpeningHour(\AppBundle\Entity\OpeningHour $openingHour)
    {
        $this->openingHours[] = $openingHour;

        return $this;
    }

    /**
     * Remove openingHour
     *
     * @param \AppBundle\Entity\OpeningHour $openingHour
     */
    public function removeOpeningHour(\AppBundle\Entity\OpeningHour $openingHour)
    {
        $this->openingHours->removeElement($openingHour);
    }

    /**
     * Get openingHours
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOpeningHours()
    {
        return $this->openingHours;
    }
    
    /**
     * Add socialNetwork
     *
     * @param \AppBundle\Entity\SocialNetwork $socialNetwork
     *
     * @return Store
     */
    public function addSocialNetwork(\AppBundle\Entity\SocialNetwork $socialNetwork)
    {
        $this->socialNetworks[] = $socialNetwork;

        return $this;
    }

    /**
     * Remove socialNetwork
     *
     * @param \AppBundle\Entity\SocialNetwork $socialNetwork
     */
    public function removeSocialNetwork(\AppBundle\Entity\SocialNetwork $socialNetwork)
    {
        $this->socialNetworks->removeElement($socialNetwork);
    }

    /**
     * Get socialNetworks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialNetworks()
    {
        return $this->socialNetworks;
    }
    
    public function __toString()
    {
        return (string) $this->getTitle();
    }
}
