<?php
namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;
/**
 * @ORM\Entity
 * @ORM\Table(name="opening_hour")
 */
class OpeningHour
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="day", type="string", length=3)
     * @Assert\NotBlank()
     */
    private $day;
    
    /**
     * @ORM\Column(name="open_hour", type="string", length=5, nullable=true)
     * @AppAssert\Hour
     */
    private $openHour;

    /**
     * @ORM\Column(name="close_hour", type="string", length=5, nullable=true)
     * @AppAssert\Hour
     */
    private $closeHour;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Store", inversedBy="openingHours", cascade={"persist"})
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id")
     * */
    private $store;    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param string $day
     *
     * @return OpeningHour
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set openHour
     *
     * @param string $openHour
     *
     * @return OpeningHour
     */
    public function setOpenHour($openHour)
    {
        $this->openHour = $openHour;

        return $this;
    }

    /**
     * Get openHour
     *
     * @return string
     */
    public function getOpenHour()
    {
        return $this->openHour;
    }

    /**
     * Set closeHour
     *
     * @param string $closeHour
     *
     * @return OpeningHour
     */
    public function setCloseHour($closeHour)
    {
        $this->closeHour = $closeHour;

        return $this;
    }

    /**
     * Get closeHour
     *
     * @return string
     */
    public function getCloseHour()
    {
        return $this->closeHour;
    }

    /**
     * Set store
     *
     * @param \AppBundle\Entity\Store $store
     *
     * @return OpeningHour
     */
    public function setStore(\AppBundle\Entity\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \AppBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }
    
    public function __toString()
    {
        return $this->getDay();
    }
}
