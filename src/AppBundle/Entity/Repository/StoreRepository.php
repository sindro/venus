<?php

namespace AppBundle\Entity\Repository;

use FOS\UserBundle\Model\UserInterface;
/**
 * StoreRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class StoreRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByDistance($radius, $lat, $long, $category = null, $tipology = null)
    {
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();

        $qb->select("s, (6373 * acos(cos
            (radians(:lat)) * cos(radians(s.latitude)) * cos(radians
            (s.longitude) - radians(:long)) + sin(radians(:lat)) * sin
            (radians(s.latitude)))) AS distance")
            ->from('AppBundle:Store', 's')
            ->having("distance < :dist")
            ->leftJoin('s.tipology', 't')
            ->leftJoin('s.categories', 'c')
            ->leftJoin('s.image', 'i')
            ->where($qb->expr()->like('s.isPublic', ':isPublic'))
            ->setParameters(array(
                ':lat' => $lat,
                ':long' => $long,
                ':lat' => $lat,
                ':dist' => $radius,
                ':isPublic' => true
            ))
            ->orderBy('distance', 'ASC');
        
        if($category && $category != 'trattamenti')
        {
            $qb->andWhere($qb->expr()->like('c.slug', ':category_slug'))->setParameter(':category_slug', $category);
        }
        
        if($tipology)
        {
            $qb->andWhere($qb->expr()->like('t.slug', ':tipology_slug'))->setParameter(':tipology_slug', $tipology);
        }
        
        return $qb->getQuery()->getResult();
    }
    
    public function findOneBySlug($slug)
    {
        $qb = $this->createQueryBuilder('s')
                    ->leftJoin('s.categories', 'c')
                    ->leftJoin('s.tipology', 't')
                    ->leftJoin('s.openingHours', 'op')
                    ->leftJoin('s.treatmentPrices', 'tp')
                    ->addSelect('c')
                    ->addSelect('t')
                    ->addSelect('op')
                    ->addSelect('tp');

        $qb->where($qb->expr()->like('s.slug', ':slug'))->setParameter('slug', $slug);
        $qb->andWhere($qb->expr()->like('s.isPublic', ':isPublic'))->setParameter('isPublic', true);

        return $qb->getQuery()->getSingleResult();
    }
    
    public function countNumberStoresForUser(UserInterface $user)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.user = :user')
            ->setParameter('user', $user)
            ->select('SUM(s.user) as numberStores')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
