<?php
namespace AppBundle\Util;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;

class Geocoder {
    
    private $requestStack;
    private $container;
    
    public function __construct(RequestStack $requestStack, ContainerInterface $container)
    {
        $this->requestStack = $requestStack;
        $this->container = $container;
    }
    
    public function getCoordinates($place)
    {
        $query = $this->requestStack->getCurrentRequest()->server->get('REMOTE_ADDR');
        $provider = 'app_geo_plugin';
            
        if($place) {
            $query = $place;
            $provider = 'app_google_maps';
        }
        
        $result = $this->container->get('bazinga_geocoder.provider.' . $provider)->geocodeQuery(GeocodeQuery::create($query));
        
        if(count($result) == 0) {
            return null;
        }
        
        return $result->first()->getCoordinates();
    }
    
    public function getLocality($lat, $long)
    {
        $result = $this->container->get('bazinga_geocoder.provider.app_google_maps')->reverseQuery(ReverseQuery::fromCoordinates($lat, $long));
        
        if(count($result) == 0) {
            return null;
        }
        
        return $result->first()->getLocality();
    }
}
