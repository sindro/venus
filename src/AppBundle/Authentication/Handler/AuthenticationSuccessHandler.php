<?php
namespace AppBundle\Authentication\Handler;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Routing\RouterInterface;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{

    private $router;
    
    public function __construct(HttpUtils $httpUtils, array $options, RouterInterface $router)
    {
        parent::__construct( $httpUtils, $options );
        $this->router = $router;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $redirect = ($request->getSession()->get('_security.main.target_path')) ? $request->getSession()->get('_security.main.target_path') : $request->server->get('HTTP_REFERER');
       
        if (strpos($redirect, '/login') !== false) {
           $redirect = $this->router->generate('fos_user_profile_show');
        }
    
        if($request->isXmlHttpRequest()) {
            $response = new JsonResponse(array('status' => 'success', 'redirect' => $redirect));
        } else {
            $response = parent::onAuthenticationSuccess($request, $token);
        }
        
        return $response;
    }
}
       