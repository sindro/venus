<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\SocialNetwork;

class SocialNetworkType extends AbstractType
{  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {         
        $builder
            ->add('social', ChoiceType::class, array(
                'label' => 'app.socialNetworks.form.social',
                'choices' => [
                    'Facebook' => 'facebook',
                    'Google Plus' => 'google-plus',
                    'Twitter' => 'twitter',
                    'Pinterest' => 'pinterest',
                    'Instagram' => 'instagram'
                ]
            ))
            ->add('url', null, array(
                'label' => 'app.form.url',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SocialNetwork::class
        ));
    }
}

