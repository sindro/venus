<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\TreatmentPriceType;
use AppBundle\Form\Type\OpeningHourType;
use AppBundle\Form\Type\SocialNetworkType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Entity\Store;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class StoreType extends AbstractType
{  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {         
        $builder
            ->add('title', null, array(
                'label' => 'app.form.title', 
            ))
            ->add('email', null, array(
                'label' => 'app.form.email',  
            ))
            ->add('website', null, array(
                'label' => 'app.form.website',  
            ))
            ->add('phone', null, array(
                'label' => 'app.form.phone',  
            ))
            ->add('address', null, array(
                'label' => 'app.form.address',  
            ))
            ->add('city', null, array(
                'label' => 'app.form.city',  
            ))
            ->add('tipology', EntityType::class, array(
                'class' => 'AppBundle:Tipology',
                'choice_label' => 'title',
                'multiple' => false,
                'expanded' => false,
                'label' => 'app.store.form.tipology',
                'attr' => ['class' => 'chosen-select']
            ))
            ->add('categories', EntityType::class, array(
                'class' => 'AppBundle:Category',
                'choice_label' => 'title',
                'multiple' => true,
                'expanded' => false,
                'label' => 'app.form.categories',
                'attr' => ['class' => 'chosen-select']
            ))
            ->add('description', 'sonata_simple_formatter_type', array(
                'format' => 'richhtml',
                'ckeditor_context' => 'default',
                'label' => 'app.form.description'
            ))
            ->add('treatmentPrices', CollectionType::class, array(
                'entry_type'   => TreatmentPriceType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr'      => array('class' => 'treatment-price-box')
                ),
            ))
            ->add('openingHours', CollectionType::class, array(
                'entry_type'   => OpeningHourType::class,
                'allow_add' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr'      => array('class' => 'opening-hour-box')
                ),
            ))
            ->add('socialNetworks', CollectionType::class, array(
                'entry_type'   => SocialNetworkType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'attr'      => array('class' => 'social-network-box')
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Store::class,
            'cascade_validation' => true
        ));
    }
}

