<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\OpeningHour;

class OpeningHourType extends AbstractType
{  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {         
        $builder
            ->add('day', ChoiceType::class, array(
                'label' => 'app.openingHour.form.day',
                'choices' => [
                    'Monday' => 'mon',
                    'Tuesday' => 'tue',
                    'Wednesday' => 'wed',
                    'Thursday' => 'thu',
                    'Friday' => 'fri',
                    'Saturday' => 'sat',
                    'Sunday' => 'sun'
                ]
            ))
            ->add('openHour', null, array(
                'label' => 'app.openingHour.form.openHour',
            ))
            ->add('closeHour', null, array(
                'label' => 'app.openingHour.form.closeHour',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OpeningHour::class
        ));
    }
}

