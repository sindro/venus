<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use AppBundle\Entity\TreatmentPrice;

class TreatmentPriceType extends AbstractType
{  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {         
        $builder
            ->add('treatment', EntityType::class, array(
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'title',
                    'label' => 'app.treatmentPrice.form.treatment',
                    'attr' => ['class' => 'chosen-select']
                ))
            ->add('shortDescription', null, array(
                'label' => 'app.treatmentPrice.form.shortDescription',
            ))
            ->add('price', null, array(
                'label' => 'app.treatmentPrice.form.price'
            ))
            ->add('reducePrice', null, array(
                'label' => 'app.treatmentPrice.form.reduce_price'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => TreatmentPrice::class
        ));
    }
}

