<?php

namespace AppIvoryBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppIvoryBundle extends Bundle
{
    public function getParent() {
        return 'IvoryCKEditorBundle';
    }
}
