CKEDITOR.plugins.add( 'pricingTable',
{
    init: function( editor )
    {
        editor.addCommand( 'insertPricingTable',
        {
            exec : function( editor )
            { 
                editor.insertHtml('<div class="pricing-container"><div class="plan"><div class="plan-price"><h3>Basic</h3><span class="value">Free</span><span class="period">Free of charge one standard listing active for 90 days</span></div><div class="plan-features"><ul><li>One Listing</li><li>90 Days Availability</li><li>Non-Featured</li><li>Limited Support</li></ul><a class="button border" href="#">Get Started</a></div></div><div class="plan featured"><div class="listing-badge"><span class="featured">Featured</span></div><div class="plan-price"><h3>Extended</h3><span class="value">$9</span><span class="period">One time fee for one listing, highlighted in search results</span></div><div class="plan-features"><ul><li>One Time Fee</li><li>One Listing</li><li>Lifetime Availability</li><li>Featured In Search Results</li><li>24/7 Support</li></ul><a class="button" href="#">Get Started</a></div></div><div class="plan"><div class="plan-price"><h3>Professional</h3><span class="value">$59</span><span class="period">Monthly subscription for unlimited listings and lifetime availability</span></div><div class="plan-features"><ul><li>Unlimited Listings</li><li>Lifetime Availability</li><li>Featured In Search Results</li><li>24/7 Support</li></ul><a class="button border" href="#">Get Started</a></div></div></div>');
            }
        });

        editor.ui.addButton( 'PricingTable',
        {
            label: 'Inserisci Tabella Prezzi',
            command: 'insertPricingTable',
            icon: this.path.replace('ivoryckeditor', 'appivory') + 'images/pricingTable.png'
        } );
    }
} );