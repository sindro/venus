CKEDITOR.plugins.add( 'tabs1',
{
    init: function( editor )
    {
        editor.addCommand( 'insertTabs1',
        {
            exec : function( editor )
            {    
                editor.insertHtml( '<div class="style-1"><ul class="tabs-nav"><li class="active"><a href="#tab1b">First Tab</a></li><li><a href="#tab2b">Second Tab</a></li><li><a href="#tab3b">Third Tab</a></li></ul><div class="tabs-container"><div class="tab-content" id="tab1b">Lorem ipsum pharetra lorem felis. Aliquam egestas consectetur elementum class aptentea taciti sociosqu ad litora torquent perea conubia nostra lorem consectetur adipiscing elit. Donec vestibulum justo a diam ultricies pellentesque.</div><div class="tab-content" id="tab2b">Aenean dolor mi, luctus et laoreet hendrerit, condimentum faucibus mi. Nam et sem eros. Sed sed eros nec massa pellentesque accumsan in nec magna. Donec sollicitudin enim nec justo mollis bibendum. Nulla eleifend mollis velit. Ut sed risus eget metus egestas sagittis. Etiam vestibulum interdum turpis.</div><div class="tab-content" id="tab3b">Suspendisse ut laoreet massa. Etiam vel dolor eu quam varius tempor eu eu mi. Duis auctor interdum ligula ut faucibus. Vivamus lorem ipsum dolor sit amet in tincidunt augue. Aenean at ligula justo, sed gravida metus. </div></div></div>' );
            }
        });

        editor.ui.addButton( 'Tabs1',
        {
            label: 'Inserisci Tabs 1',
            command: 'insertTabs1',
            icon: this.path.replace('ivoryckeditor', 'appivory') + 'images/tabs1.png'
        } );
    }
} );