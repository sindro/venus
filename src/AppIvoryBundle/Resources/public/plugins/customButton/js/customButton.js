CKEDITOR.plugins.add( 'customButton', {
    init: function( editor ) {
        editor.addCommand( 'insertCustomButton', new CKEDITOR.dialogCommand( 'customButtonDialog' ) );
        editor.ui.addButton( 'CustomButton', {
            label: 'Inserisci Pulsante',
            command: 'insertCustomButton',
            icon: this.path.replace('ivoryckeditor', 'appivory') + 'images/button.png'
        });

        CKEDITOR.dialog.add( 'customButtonDialog', this.path.replace('ivoryckeditor', 'appivory') + 'dialogs/button.js' );
    }
});