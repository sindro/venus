CKEDITOR.dialog.add( 'customButtonDialog', function( editor ) {
    return {
        title: 'Proprietà Pulsante',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Impostazioni',
                elements: [
                    {
                        type: 'text',
                        id: 'icon',
                        label: 'Icona',
                    },
                    {
                        type: 'text',
                        id: 'title',
                        label: 'Nome Pulsante',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Il campo non può essere vuoto." )
                    },
                    {
                        type: 'text',
                        id: 'link',
                        label: 'Link',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Il campo non può essere vuoto." )
                    },
                    {
                        type: 'select',
                        id: 'target',
                        label: 'Target Link',
                        items: [ [ '_self' ], [ '_blank' ] ],
                        'default': '_self',
                    },
                    {
                        type: 'checkbox',
                        id: 'border',
                        label: 'Effetto Bordo?',
                    },
                    {
                        type: 'checkbox',
                        id: 'medium',
                        label: 'Dimensioni Medie?',
                    }
                ]
            }
        ],
        onOk: function() {
            var dialog = this;

            var button = editor.document.createElement( 'customButton' );
            var title = dialog.getValueOf( 'tab-basic', 'title' );
            var link = dialog.getValueOf( 'tab-basic', 'link' );
            var target = dialog.getValueOf( 'tab-basic', 'target' );
            var icon = dialog.getValueOf( 'tab-basic', 'icon' ) ? dialog.getValueOf( 'tab-basic', 'icon' ) : '';
            
            var medium = dialog.getValueOf( 'tab-basic', 'medium' ) ? ' medium' : '';
            var css_class = dialog.getValueOf( 'tab-basic', 'border' ) ? 'button border' + medium : 'button' + medium;
            
            editor.insertHtml( '<a target="' + target + '" href="' + link + '" class="' + css_class + '">' + icon +  title + '</a>' );
        }
    };
});