CKEDITOR.plugins.add( 'toggle',
{
    init: function( editor )
    {
        editor.addCommand( 'insertToggle',
        {
            exec : function( editor )
            {    
                editor.insertHtml('<div class="style-2"><div class="toggle-wrap"><span class="trigger "><a href="#">First Toggle<i class="sl sl-icon-plus"></i></a></span><div class="toggle-container"><p>Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. Donec ut volutpat metus. Vivamus justo arcu, elementum a sollicitudin pellentesque, tincidunt ac enim. Proin id arcu ut ipsum vestibulum elementum.</p></div></div><div class="toggle-wrap"><span class="trigger"><a href="#"><i class="sl sl-icon-pin"></i>Second Toggle With Icon <i class="sl sl-icon-plus"></i> </a></span><div class="toggle-container"><p>Seded ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. Donec ut volutpat metus. Aliquam tortor lorem, fringilla tempor dignissim at, pretium et arcu.</p></div></div></div>');
            }
        });

        editor.ui.addButton( 'Toggle1',
        {
            label: 'Inserisci Toggle',
            command: 'insertToggle',
            icon: this.path.replace('ivoryckeditor', 'appivory') + 'images/toggle.png'
        } );
    }
} );